    /**
     * my Ajax object
     */
    var com_yung_util_AjaxHttp = $Class.extend({
        classProp : { name : "com.yung.util.AjaxHttp"},
        actionUrl : null,
        formId : null,
        callback : null,
        loading : null,
        showError : null,
        type : 'POST',
        dataType : 'json',
        ajaxLoadingShow : null,
        ajaxLoadingHide : null,
        init: function(actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide){
            this.actionUrl = actionUrl;
            if (formId) this.formId = formId;
            if (callback) this.callback = callback;
            this.loading = true;
            this.showError = true;
            if (typeof ajaxLoadingShow == 'function' && typeof ajaxLoadingHide == 'function') {
                // customized loading and function
                this.ajaxLoadingShow = ajaxLoadingShow;
                this.ajaxLoadingHide = ajaxLoadingHide;
            } else {
                // use standard loading
                com_yung_util_AjaxHttp.createLoadingDiv();
                var agt = navigator.userAgent.toLowerCase();
                if (agt.indexOf('msie') != -1) {
                    com_yung_util_AjaxHttp.setLoadingText("Loading ...");
                }
                this.ajaxLoadingShow = function () {
                    var windowHeight = jQuery(document).height();
                    if (jQuery("#loadingCSS-imgDiv")[0] != null) {
                        jQuery("#loadingCSS").css("height", windowHeight + "px");
                        var blinkTxt = jQuery("#text-blink")[0];
                        if (blinkTxt != null) {
                            var blinkTable = jQuery("#text-blink-table");
                            var pos = new com.yung.util.Position(blinkTable);
                            var baseTop = pos.getBaseTop();
                            blinkTable[0].style.top = (baseTop + 300);
                            if (window['console'] == null){
                                window.scrollTo(0, 0);
                            }
                            yung_global_var["_global_text_blink"] = setInterval(function() {
                                if (window['console'] != null) {
                                    console.log("blink!");
                                }
                                blinkTxt.style.display = (blinkTxt.style.display == 'none' ? '' : 'none');
                            }, 750); 
                        }
                    } else {
                        var windowWidth = jQuery(document).width();
                        jQuery("#yung-backgroundDiv").css("width", windowWidth + "px");
                        jQuery("#yung-backgroundDiv").css("height", windowHeight + "px");
                    }
                    jQuery("#loadingCSS").css("display", "block");
                };
                this.ajaxLoadingHide = function () {
                    jQuery("#loadingCSS").css("display", "none");
                    var blinkTxt = jQuery("#text-blink")[0];
                    if (blinkTxt != null) {
                        clearInterval(yung_global_var["_global_text_blink"]);
                    }
                };
            }
            return this;
        },
        beforeSend : function() {
            if (this.loading) {
                if (yung_global_var["_global_ajaxTimeout"] == null) {
                    yung_global_var["_global_ajaxTimeout"] = setTimeout(this.ajaxLoadingShow, 500);
                } else {
                    clearTimeout(yung_global_var["_global_ajaxTimeout"]);
                    delete yung_global_var["_global_ajaxTimeout"];
                    yung_global_var["_global_ajaxTimeout"] = setTimeout(this.ajaxLoadingShow, 500);
                }
            }
        },
        sendComplete : function () {
            clearTimeout(yung_global_var["_global_ajaxTimeout"]);
            delete yung_global_var["_global_ajaxTimeout"];
            this.ajaxLoadingHide();
        },
        setLoading : function(loading){
            this.loading = loading;
        },
        setShowError : function(showErr){
            this.showError = showErr;
        },
        send : function(caller){
            if (this.actionUrl == null) {
                alert("please define actionUrl!");
                return;
            }
            // show load.gif if more than half second
            this.beforeSend();
            
            var params = "";
            // param serialize
            if (this.formId != null) params = jQuery('#' + this.formId).serialize();
            //alert("params ok");
            
            var callback = this.callback;
            var self = this;
            
            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                error: function(x, status, error) {
                    self.sendComplete();
                    if (self.showError) alert("An ajax error occurred: " + status + " Error: " + error);
                },
                url: this.actionUrl,
                data: params,
                jsonpCallback: callback,
                success:function(result){
                    self.sendComplete();
                    this.jsonpCallback(result, self, caller);
                }
            });
        }
    });
    com_yung_util_AjaxHttp.createLoadingDiv = function () {
        var loading = jQuery("#loadingCSS");
        if (loading[0] == null) {
            var windowHeight = jQuery(document).height();
            var windowWidth = jQuery(document).width();
            var template = new com.yung.util.BasicTemplate();
            var data = {};
            data["windowHeight"] = windowHeight;
            data["windowWidth"] = windowWidth;
            template.add('<div id="loadingCSS" style="display:none" tabindex="1" >');
            template.add('    <div id="yung-backgroundDiv" style="width: {{windowWidth}}px; height: {{windowHeight}}px; position: absolute; text-align: left: 0px; top: 0px; width: 100%; height: 2000px; center; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);" >');
            template.add('        <table class="loading_gif_center" >');
            template.add('            <tr><td>');
            template.add('                <div id="loadingCSS-content" class="loader">Loading...</div>');
            template.add('            </td></tr>');
            template.add('        </table>');
            template.add('    </div>');
            template.add('</div>');
            jQuery("body").append(template.toHtml(data));
        }
    };
    com_yung_util_AjaxHttp.setLoadingText = function (text) {
        if (jQuery("#loadingCSS-imgDiv")[0] == null) {
            jQuery("#loadingCSS").attr("style", "display: none; position: absolute; text-align: left: 0px; top: 0px; width: 100%; height: 200px; center; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);");
            var template = new com.yung.util.BasicTemplate();
            var data = {};
            data["text"] = text;
            template.add('<table id="text-blink-table" style="position: absolute; width: 100%;" align="center" >');
            template.add('    <tr><td align="center" >');
            template.add('        <div id="loadingCSS-imgDiv" style="">');
            template.add('            <span id="text-blink">{{text}}</span>');
            template.add('        </div>');
            template.add('    </td></tr>');
            template.add('</table>');
            jQuery("#loadingCSS").html(template.toHtml(data));
        }
    };
    com_yung_util_AjaxHttp.setImage = function (imgSrc) {
        if (jQuery("#loadingCSS-imgDiv")[0] == null) {
            jQuery("#loadingCSS").attr("style", "display: none; position: absolute; text-align: left: 0px; top: 0px; width: 100%; height: 200px; center; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);");
            var template = new com.yung.util.BasicTemplate();
            var data = {};
            data["imgSrc"] = imgSrc;
            template.add('<table class="loading_gif_center" style="width: 100%; height: 100%;" >');
            template.add('    <tr><td align="center" >');
            template.add('        <div id="loadingCSS-imgDiv" style="">');
            template.add('            <img id="pageLoading" src="{{imgSrc}}" />');
            template.add('        </div>');
            template.add('    </td></tr>');
            template.add('</table>');
            jQuery("#loadingCSS").html(template.toHtml(data));
        }
    };
    
    /**
     * my Ajax upload with multiple files
     */
    var com_yung_util_AjaxUpload = com_yung_util_AjaxHttp.extend({
        classProp : { name : "com.yung.util.AjaxUpload"},
        init : function (actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide) {
            this._super(actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide);
            return this;
        },
        send : function (caller) {
            if (this.actionUrl == null) {
                alert("please define action!");
                return;
            }
            // show load.gif if more than half second
            this.beforeSend();
            
            var formData = new FormData();
            jQuery("#" + this.formId + " :input").each(function(key, input){
                var name = jQuery(this).attr("name");
                var type = jQuery(this).attr("type");
                if ("file" == type) {
                    var files = input.files;
                    var i = 0;
                    var file = files[i];
                    while (file != null) {
                        formData.append(name, file);
                        i++;
                        file = files[i];
                    }
                } else if ("button" != type && "submit" != type && name != null && name != '') {
                    formData.append(name, input.value);
                }
            });
            
            var callback = this.callback;
            var self = this;
            
            jQuery.ajax({
                type:'POST',
                dataType: 'json',
                data: formData,
                processData: false,
                contentType: false,
                error: function(x, status, error) {
                    clearTimeout(yung_global_var._global_ajaxTimeout);
                    self.ajaxLoadingHide();
                    if (self.showError) alert("An ajax error occurred: " + status + " Error: " + error);
                },
                url: this.actionUrl,
                jsonpCallback: callback,
                success:function(result){
                    clearTimeout(yung_global_var._global_ajaxTimeout);
                    self.ajaxLoadingHide();
                    this.jsonpCallback(result, self, caller);
                }
            });
        }
    });
    
    jQuery( document ).ready(function() {
        if (typeof com_yung_util_BasicTemplate == 'undefined') {
            alert("yung-AJAX.js requires yung-BasicTemplate.js!");
        }
        if (typeof com_yung_util_Position == 'undefined') {
            alert("yung-AJAX.js requires yung-Position.js!");
        }
    });    