    var com_yung_util_Calendar = $Class.extend({
        classProp : { name : "com.yung.util.Calendar" },
        d : null,
        init : function(arg1, arg2, arg3, arg4, arg5, arg6, arg7) {
            if (arg1 instanceof Date) {
                this.d = arg1;
                return this;
            }
            if (arg1 == null) arg1 = 0;
            if (arg2 == null) arg2 = 0;
            if (arg3 == null) arg3 = 0;
            if (arg4 == null) arg4 = 0;
            if (arg5 == null) arg5 = 0;
            if (arg6 == null) arg6 = 0;
            if (arg7 == null) arg7 = 0;
            if (typeof arg1 != "number") {
                throw "Year should be integer";
            }
            if (typeof arg2 != "number") {
                throw "Month should be integer";
            }
            if (typeof arg3 != "number") {
                throw "Day should be integer";
            }
            if (typeof arg4 != "number") {
                throw "Hours should be integer";
            }
            if (typeof arg6 != "number") {
                throw "Minutes should be integer";
            }
            if (typeof arg7 != "number") {
                throw "Milliseconds should be integer";
            }
            var year, month, day, hour, min, sec, milli;
            year = parseInt(arg1);
            month = parseInt(arg2) - 1;
            if (month < 0) month = 0;
            day = parseInt(arg3);
            hour = parseInt(arg4);
            min = parseInt(arg5);
            sec = parseInt(arg6);
            milli = parseInt(arg7);
            var d = new Date();
            d.setYear(year);
            d.setMonth(month);
            d.setDate(day);
            d.setHours(hour);
            d.setMinutes(min);
            d.setSeconds(sec);
            d.setMilliseconds(milli);
            this.d = d;
            return this;
        },
        getYear : function () {
            return this.d.getFullYear();
        },
        getMonth : function () {
            return this.d.getMonth();
        },
        getDate : function () {
            return this.d.getDate();
        },
        getHours : function () {
            return this.d.getHours();
        },
        getMinutes : function () {
            return this.d.getMinutes();
        },
        getSeconds : function () {
            return this.d.getSeconds();
        },
        getMilliseconds : function () {
            return this.d.getMilliseconds();
        },
        getDay : function () {
            return this.d.getDay();
        },
        setYear : function (year) {
            this.d.setFullYear(year);
        },
        setMonth : function (month) {
            return this.d.setMonth(month);
        },
        setDate : function (day) {
            return this.d.setDate(day);
        },
        setHours : function (hours) {
            return this.d.setHours(hours);
        },
        setMinutes : function (min) {
            return this.d.setMinutes(min);
        },
        setSeconds : function (sec) {
            return this.d.setSeconds(sec);
        },
        setMilliseconds : function (milli) {
            return this.d.setMilliseconds(milli);
        },
        addYear : function (year) {
            this.d.setFullYear(year + this.getYear());
        },
        addMonth : function (month) {
            return this.d.setMonth(month + this.getMonth());
        },
        addDay : function (day) {
            return this.d.setDate(day + this.getDate());
        },
        addHours : function (hours) {
            return this.d.setHours(hours + this.getHours());
        },
        addMinutes : function (min) {
            return this.d.setMinutes(min + this.getMinutes());
        },
        addSeconds : function (sec) {
            return this.d.setSeconds(sec + this.getSeconds());
        },
        addMilliseconds : function (milli) {
            return this.d.setMilliseconds(milli + this.getMilliseconds());
        },
        getTime : function () {
            return this.d.getTime();
        },
        setTime : function (time) {
            if (typeof time != 'number') {
                throw "argument time should be integer";
            }
            time = parseInt(time);
            this.d = new Date(time);
        },
        toDate : function () {
            return new Date(this.d.getTime());
        },
        toString : function () {
            return this.d.toString();
        }
    });
    
    var com_yung_util_SimpleDateFormat = $Class.extend({
        classProp : { name : "com.yung.util.SimpleDateFormat", year : "yyyy", month : "MM", date : "dd", hour : "HH", min : "mm", sec : "ss" },
        dateFormat : null,
        init : function(arg1) {
            if (typeof arg1 != 'string') {
                throw "argument type is not string";
            }
            this.dateFormat = arg1;
        },
        replaceAll : function(targetStr, strFind, strReplace) {
            var index = 0;
            while (targetStr.indexOf(strFind, index) != -1) {
                targetStr = targetStr.replace(strFind, strReplace);
                index = targetStr.indexOf(strFind, index);
            }
            return targetStr;
        },
        parse : function (dateString) {
            var year = 0;
            var month = 0;
            var date = 0;
            var hour = 0;
            var min = 0;
            var sec = 0;
            var milli = 0;
            for (var key in this.classProp) {
                var match = this.classProp[key];
                if (key == 'year') {
                    var start = this.dateFormat.indexOf(match);
                    if (start >= 0) {
                        var end = start + match.length;
                        year = parseInt(dateString.substring(start, end));
                    }
                } else if (key == 'month') {
                    var start = this.dateFormat.indexOf(match);
                    if (start >= 0) {
                        var end = start + match.length;
                        month = parseInt(dateString.substring(start, end));
                    }
                } else if (key == 'date') {
                    var start = this.dateFormat.indexOf(match);
                    if (start >= 0) {
                        var end = start + match.length;
                        date = parseInt(dateString.substring(start, end));
                    }
                } else if (key == 'hour') {
                    var start = this.dateFormat.indexOf(match);
                    if (start >= 0) {
                        var end = start + match.length;
                        hour = parseInt(dateString.substring(start, end));
                    }
                } else if (key == 'min') {
                    var start = this.dateFormat.indexOf(match);
                    if (start >= 0) {
                        var end = start + match.length;
                        min = parseInt(dateString.substring(start, end));
                    }
                } else if (key == 'sec') {
                    var start = this.dateFormat.indexOf(match);
                    if (start >= 0) {
                        var end = start + match.length;
                        sec = parseInt(dateString.substring(start, end));
                    }
                }
            }
            var cal = new com.yung.util.Calendar(year, month, date, hour, min, sec, milli);
            return cal;
        },
        paddingZero : function (str, total) {
            var diff = total - str.length;
            if (diff > 0) {
                var ret = "";
                for (var i = 0; i < diff; i++) {
                    ret = ret + "0";
                }
                ret = ret + str;
                return ret;
            } else {
                return str;
            }
        },
        format : function (date) {
            var cal = null;
            if (date instanceof Date) {
                cal = new com.yung.util.Calendar(date);
            } else if (date instanceof com.yung.util.Calendar) {
                cal = date;
            } else {
                throw "argument date is not a Date or com.yung.util.Calendar";
            }
            var ret = "" + this.dateFormat;
            for (var key in this.classProp) {
                if (key == 'year') {
                    ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getYear() + "", 4));
                } else if (key == 'month') {
                    ret = this.replaceAll(ret, this.classProp[key], this.paddingZero((cal.getMonth() + 1) + "", 2));
                } else if (key == 'date') {
                    ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getDate() + "", 2));
                } else if (key == 'hour') {
                    ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getHours() + "", 2));
                } else if (key == 'min') {
                    ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getMinutes() + "", 2));
                } else if (key == 'sec') {
                    ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getSeconds() + "", 2));
                }
            }
            return ret;
        }
    });