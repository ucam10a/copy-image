    var com_yung_util_FloatIframe = $Class.extend({
        classProp : { name : "com.yung.util.FloatIframe" },
        divId : null,
        init : function (divId) {
            if (yung_global_var["com_yung_util_FloatIframe-" + divId] != null) {
                throw "Please use com.yung.util.FloatIframe.instance() to create instance!";
            }
            this.divId = divId;
            com_yung_util_FloatIframe.instance(this);
            this.createFloatIFrame();
            return this;
        },
        bindCloseEvent : function (close, element, event) {
            if (element == null) {
                element = jQuery("body");
            }
            if (event == null) {
                event = "click";
            }
            if (typeof close == 'function') {
                element.bind( event, function() {
                    close();
                });    
            }
        },
        createFloatIFrame : function () {
            var floatDiv = jQuery("#yung-FloatIframe-" + this.divId)[0];
            if (floatDiv == null) {
                var obj = {};
                obj["divId"] = this.divId;
                var template = new com.yung.util.BasicTemplate(obj);
                template.add('<div id="yung-FloatIframe-{{divId}}" class="float-border" style="position: absolute; left: 0px; top: 0px; margin: 3px; display: none; background: white; box-shadow: 5px 5px 5px #d7d7d7; width: 500px; width:300px; z-index: 5000;">');
                template.add('    <div id="yung-FloatIframe-loading-{{divId}}" style="width: 100%; height: 100%; display: block;">');
                template.add('        <table width="100%" height="100%">');
                template.add('            <tr>');
                template.add('                <td align="center"><div class="loader">Loading...</div></td>');
                template.add('            </tr>');
                template.add('        </table>');
                template.add('    </div>');
                template.add('    <div id="yung-FloatIframe-container-{{divId}}" style="display: none; width: 100%; height: 100%;">');
                template.add('        <div style="position: absolute; right: 2px; top: 0px; cursor: pointer;"><span style="color: red; background-color: white;" title="close" onclick="com_yung_util_FloatIframe.instance(\'{{divId}}\').showFloatIframe(false, \'{{divId}}\');" style="cursor: pointer;" >&#10006;</span></div>');
                template.add('        <iframe id="yung-FloatIframe-content-{{divId}}" src="" width="100%" frameborder="0" style="height: 100%; border-style: none transparent;" onload="com_yung_util_FloatIframe.instance(\'{{divId}}\').showFloatContent();"></iframe>');
                template.add('    </div>');
                template.add('</div>');
                var html = template.toHtml();
                jQuery("body").append(html);
            }
        },
        showFloatContent : function () {
            jQuery("#yung-FloatIframe-loading-" + this.divId).css("display", "none");
            jQuery("#yung-FloatIframe-container-" + this.divId).css("display", "block");
        },
        showFloatIframe : function (show, divId) {
            if (divId == null) {
                divId = this.divId;
            }
            if (show == false) {
                var display = jQuery("#yung-FloatIframe-" + divId).css("display");
                if (display != 'none') {
                    jQuery("#yung-FloatIframe-" + divId).css("display", "none");
                }
                jQuery("yung-FloatIframe-loading" + divId).css("display", "block");
                return; 
            }
            var display = jQuery("#yung-FloatIframe-" + divId).css("display");
            if (display != 'block') {
                jQuery("#yung-FloatIframe-" + divId).css("display", "block");
            }
        },
        closeFloatIframe : function (callback) {
            this.showFloatIframe(false);
            if (typeof callback === "function") {
                callback();
            }   
        },
        openFloatIframe : function (url, pos, width, height) {
            if (width == null) {
                width = 500;
            }
            if (height == null) {
                height = 300;
            }
            if (pos == null) {
                alert("please specify position!");
            }
            var top = pos.top + "px";
            var left = pos.left + "px";
            jQuery("#yung-FloatIframe-" + this.divId).css("width", width);
            jQuery("#yung-FloatIframe-" + this.divId).css("height", height);
            jQuery("#yung-FloatIframe-" + this.divId).css("left", left);
            jQuery("#yung-FloatIframe-" + this.divId).css("top", top);
            document.getElementById('yung-FloatIframe-content-' + this.divId).src = url;
            // use setTimeout to prevent body click event conflict
            var show = this.showFloatIframe;
            var divId = this.divId;
            setTimeout(function(){ show(true, divId); }, 300);
        }
    });
    com_yung_util_FloatIframe.instance = function(floatIframe) {
        if (floatIframe != null) {
            if (floatIframe instanceof com_yung_util_FloatIframe) {
                yung_global_var["com_yung_util_FloatIframe" + floatIframe.divId] = floatIframe;
            }
            if (typeof floatIframe == 'string') {
                var instance = yung_global_var["com_yung_util_FloatIframe" + floatIframe];
                if (instance == null) {
                    instance = new com_yung_util_FloatIframe(floatIframe);
                }
                return instance;
            }
        }
    }
    jQuery( document ).ready(function() {
        if (typeof com_yung_util_BasicTemplate == 'undefined') {
            alert("yung-FloatIframe.js requires yung-BasicTemplate.js!");
        }
    });