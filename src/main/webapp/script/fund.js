function submitenter(e, exeFunction, param1, param2, param3, param4, param5) {
    var keycode = null;
    if (window.event) {
    	keycode = window.event.keyCode;
    } else if (e) {
    	keycode = e.which;
    } else {
    	return true;
    }
    if (keycode == 13) {
    	exeFunction(param1, param2, param3, param4, param5);
        return false;
    } else {
    	return true;
    }
}

function buyFund(){
	var fundName = document.getElementById("fundName").value;
    var md5FundName = MD5Util.calc(fundName);
    document.getElementById("md5FundName").value = md5FundName;
    var fundPrice = $("#fund-price").val();
    var fundUnit = $("#fund-unit").val();
    var buyDate = $("#fund-buyDate").val();
    var exchangeRate = $("#fund-exchange").val();
    var validator = new Validator(submitBuyFundFrom);
    if (fundPrice == null || fundPrice == "") {
    	alert("\u57FA\u91D1\u50F9\u683C\u4E0D\u80FD\u70BA\u7A7A\u503C");
        return;
    }
    if (fundUnit == null || fundUnit == "") {
    	alert("\u57FA\u91D1\u55AE\u4F4D\u4E0D\u80FD\u70BA\u7A7A\u503C");
    	return;
    }
    if (buyDate == null || buyDate == "") {
    	alert("\u8CFC\u8CB7\u65E5\u671F\u4E0D\u80FD\u7A7A\u767D");
    	return;
    }
    if (exchangeRate == null || exchangeRate == "") {
        alert("\u8ACB\u8F38\u5165\u532F\u7387");
        return;
    }
    validator.initNumberParam(fundPrice, fundUnit, exchangeRate);
    validator.initDateParam(buyDate);
    validator.validate();
}

function submitBuyFundFrom(){
	document.buyFundFrom.submit();
}

function changeDate(ele) {
	var day = ele.value;
	if (day == null || day == '') return;
	$("#showAllFundByDayForm-day").val(day);
	var validator = new Validator(submitShowAllFundFrom);
    if (day == null || day == "") {
    	alert("\u8ACB\u8F38\u5165\u65E5\u671F");
        return;
    }
    validator.initDateParam(day);
    validator.validate();
}

function showAllFund(){
	var day = $("#showAllFund-date").val();
	$("#showAllFundByDayForm-day").val(day);
	var validator = new Validator(submitShowAllFundFrom);
    if (day == null || day == "") {
    	alert("\u8ACB\u8F38\u5165\u65E5\u671F");
        return;
    }
    validator.initDateParam(day);
    validator.validate();
}

function submitShowAllFundFrom(){
	document.showAllFundByDayForm.submit();
}

function editExchange(currency) {
	$("#updateExchangeHistory-currency").val(currency);
    var rate = $("#" + currency + "-exchangeRate").val();
    if (rate == null || rate == '') {
        alert("\u5C1A\u672A\u8F38\u5165");
        return;
    }
    $("#updateExchangeHistory-rate").val(rate);
    var validator = new Validator(updateExchangeHistory);
    validator.initNumberParam(rate);
    validator.validate();
}

function updateExchangeHistory() {
    document.updateExchangeHistoryForm.submit();
}

function editFundPrice(id, md5FundName) {
	$("#updateFundPrice-md5FundName").val(md5FundName);
    var price = document.getElementById(md5FundName + "-fundPrice").value;
    if (price == null || price == '') {
        alert("\u5C1A\u672A\u8F38\u5165");
        return;
    }
    $("#updateFundPrice-fundPrice").val(price);
    var validator = new Validator(updateFundPrice);
    validator.initNumberParam(price);
    validator.validate();
}

function updateFundPrice() {
    document.updateFundPriceForm.submit();
}

function editFundUnit(id) {
    $("#updateFundUnit-id").val(id);
    var unit = document.getElementById(id + "-fundUnit").value;
    if (unit == null || unit == '') {
        alert("\u5C1A\u672A\u8F38\u5165");
        return;
    }
    $("#updateFundUnit-fundUnit").val(unit);
    var validator = new Validator(updateFundUnit);
    validator.initNumberParam(unit);
    validator.validate();
}

function updateFundUnit() {
    document.updateFundUnitForm.submit();
}

function selectFundName(selectTag, targetId){
	if (targetId == null) {
		document.getElementById("fundName").value = selectTag.value;
	} else {
		document.getElementById(targetId).value = selectTag.value;
	}
}

function selectCurrency(selectTag){
	document.getElementById("fundName").value = selectTag.value;
}

function redirect(url) {
    window.location.href = url;
}

function editFundComment(id) {
	var comment = document.getElementById(id + "-comment-text").innerHTML;
	$("#comment-editor-comment").val(comment);
	$("#comment-editor-fundId").val(id);
	$("#editCommentDialog").dialog("open");
}

function submitFundComment(soldHistory) {
	var comment = $("#comment-editor-comment").val();
	var id = $("#comment-editor-fundId").val();
	$("#updateFundComment-id").val(id);
	$("#updateFundComment-comment").val(comment);
	if (soldHistory == true) {
	    var ajaxhttp = new com.yung.util.AjaxHttp('updateFundCommentAjax', 'updateFundCommentForm', function callback(response, self)  
	    {
	        var errormsg = response.errormsg;
	        if (errormsg != '') {
	            alert(errormsg);
	        } else {
	            window.location.href = 'showAllSoldFundHistory.action';
	        }
	    });
	    ajaxhttp.send();
	    return;
	}
	document.updateFundCommentForm.submit();
}

function changePass() {
	var ajaxhttp = new com.yung.util.AjaxHttp('changePass', 'changePassForm', function callback(response, self)  
    {
        var errormsg = response.errormsg;
        if (errormsg != '') {
        	alert(errormsg);
            if (errormsg == '\u66F4\u6539\u5BC6\u78BC\u6210\u529F') {
            	window.location.href = 'showAllFund.action';
            }
        }
    });
    ajaxhttp.send();
}

function moveFund(id, fundName) {
	answer = confirm(fundName + "\u79FB\u81F3\u6B77\u53F2\u5340");
	if (!answer) {
		return;
	}
	$("#delFund-id").val(id);
	var nowDay = $("#nowDay").val();
	$("#delFund-moveDate").val(nowDay);
	var ajaxhttp = new com.yung.util.AjaxHttp('moveFund', 'delFundForm', function callback(response, self)  
    {
        var errormsg = response.errormsg;
        if (errormsg != '') {
        	alert(errormsg);
        } else {
        	window.location.href = 'showAllFund.action';
        }
    });
    ajaxhttp.send();
}

function delFund(id, fundName) {
    answer = confirm("\u78BA\u8A8D\u522A\u9664" + fundName + " ?");
    if (!answer) {
        return;
    }
    $("#delFund-id").val(id);
    var ajaxhttp = new com.yung.util.AjaxHttp('delFund', 'delFundForm', function callback(response, self)  
    {
        var errormsg = response.errormsg;
        if (errormsg != '') {
            alert(errormsg);
        } else {
            window.location.href = 'showAllFund.action';
        }
    });
    ajaxhttp.send();
}

function inputSellFund(id, fundName, unit, buyDate, price, exchange) {
	if (checkExchangeRate() == false) {
		return;
	}
	var currentValue = $("#" + id + "-currentValue-hidden").val();
	if (currentValue == null || currentValue == '') {
		alert('\u8ACB\u8F38\u5165 ' + fundName + ' \u6DE8\u503C\u53CA\u55AE\u4F4D\u6578');
		return;
	}
	document.getElementById("sellFund-name").innerHTML = fundName;
	if (unit == null || unit == '') {
		unit = $('#' + id + "-fundUnit").val();
	}
	document.getElementById("sellFund-buyDate").innerHTML = buyDate;
	document.getElementById("sellFund-price").innerHTML = price;
	document.getElementById("sellFund-exchange").innerHTML = exchange;
	document.getElementById("sellFund-unit").innerHTML = unit;
	document.getElementById("max-sell-fund-unit").innerHTML = unit;
	document.getElementById("sell-fund-id").value = id;
	$("#sellFundDialog").dialog("open");
}

function inputChangeFund(id, fundName, unit, buyDate, price, exchange) {
	if (checkExchangeRate() == false) {
		return;
	}
	var currentValue = $("#" + id + "-currentValue-hidden").val();
	if (currentValue == null || currentValue == '') {
		alert('\u8ACB\u8F38\u5165 ' + fundName + ' \u6DE8\u503C\u53CA\u55AE\u4F4D\u6578');
		return;
	}
	document.getElementById("changeFund-name").innerHTML = fundName;
	if (unit == null || unit == '') {
		unit = $('#' + id + "-fundUnit").val();
	}
	var currencyName = $('#' + id + "-fundCurrency").html();
	document.getElementById("changeFund-unit").innerHTML = unit;
	document.getElementById("changeFund-buyDate").innerHTML = buyDate;
	document.getElementById("changeFund-price").innerHTML = price;
	document.getElementById("changeFund-currency").innerHTML = currencyName;
	document.getElementById("changeFund-currentValue").innerHTML = currentValue;
	document.getElementById("change-fund-id").value = id;
	$('#change-origin-name').html(fundName);
	$('#change-origin-currency').html(currencyName);
	$("#changeFundDialog").dialog("open");
}

function checkExchangeRate() {
	var result = true;
	$("[id$='-exchangeRate']").each(function(index) {
		var rate = $(this).val();
		if (rate == null || rate == '') {
			alert("\u8ACB\u8F38\u5165\u7576\u65E5\u532F\u7387");
			result = false;
		}
	});
	return result;
}

function sellFund() {
	document.getElementById("sellFundForm-id").value = document.getElementById("sell-fund-id").value;
	document.getElementById("sellFundForm-unit").value = document.getElementById("sell-fund-unit").value;
	document.getElementById("sellFundForm-sellDate").value = $('#nowDay').val();
	var unit = document.getElementById("sellFundForm-unit").value;
	var sellDate = document.getElementById("sellFundForm-sellDate").value;
	var validator = new Validator(sellFundSubmit);
    validator.initNumberParam(unit);
    validator.initDateParam(sellDate);
    validator.validate();
}

function sellFundSubmit() {
	var ajaxhttp = new com.yung.util.AjaxHttp('sellFund', 'sellFundForm', function callback(response, self)  
    {
        var errormsg = response.errormsg;
        if (errormsg != '') {
        	alert(errormsg);
        } else {
        	window.location.href = 'showAllFund.action';
        }
    });
	ajaxhttp.send();
}

function changeFund() {
	document.getElementById("changeFundForm-id").value = document.getElementById("change-fund-id").value;
	document.getElementById("changeFundForm-name").value = document.getElementById("change-fund-name").value;
	document.getElementById("changeFundForm-value").value = document.getElementById("change-fund-value").value;
	document.getElementById("changeFundForm-price").value = document.getElementById("change-fund-price").value;
	document.getElementById("changeFundForm-currency").value = document.getElementById("change-fund-currency").value;
	document.getElementById("changeFundForm-date").value = $('#nowDay').val();
	var buyFundName = document.getElementById("changeFundForm-name").value;
	var buyFundMd5 = MD5Util.calc(buyFundName);
	document.getElementById("changeFundForm-md5").value = buyFundMd5;
	var value = document.getElementById("changeFundForm-value").value;
	var price = document.getElementById("changeFundForm-price").value;
	var changeDate = document.getElementById("changeFundForm-date").value;
	var validator = new Validator(changeFundSubmit);
    validator.initNumberParam(value, price);
    validator.initDateParam(changeDate);
    validator.validate();
}

function changeFundSubmit() {
	var ajaxhttp = new com.yung.util.AjaxHttp('changeFund', 'changeFundForm', function callback(response, self)  
    {
        var errormsg = response.errormsg;
        if (errormsg != '') {
        	alert(errormsg);
        } else {
        	window.location.href = 'showAllFund.action';
        }
    });
	ajaxhttp.send();
}

function changeCurrency(selectTag, targetId){
	var currency = selectTag.value;
	if (targetId == null) {
		if (currency == 'taiwan') {
			$("#exchange-rate").val("1.0");
		} else {
			$("#exchange-rate").val("");
		}
	} else {
		if (currency == 'taiwan') {
			$("#" + targetId).val("1.0");
		} else {
			$("#" + targetId).val("");
		}
	}
}

function showFundBalanceByName(){
	var ajaxhttp = new com.yung.util.AjaxHttp('showAllFundAjax', null, function callback(response, self)  
    {
        var errormsg = response.errormsg;
        if (errormsg != '') {
        	alert(errormsg);
        } else {
        	var ready = response.ready;
        	if (ready == true){
        		window.location.href = 'showFundBalanceByName.action';
        	} else {
        	    alert("\u8ACB\u8F38\u5165\u4ECA\u65E5\u532F\u7387\u53CA\u6240\u6709\u57FA\u91D1\u50F9\u683C");	
        	}
        }
    });
	ajaxhttp.send();
}

function modifyFundDate(date){
	$("#displayFundDate").html("");
	$("#changeFundDate").html('<input type="text" size=12 value="' + date + '" onKeyPress="return submitenter(event, modifyDate, this)" />');
}

function modifyDate(dateTag) {
	answer = confirm("\u662F\u5426\u5C07\u76EE\u524D" + $("#nowDay").val() + "\u6240\u6709\u8CC7\u6599\u5168\u90E8\u79FB\u81F3" + dateTag.value + " ?");
	if (!answer) {
		return;
	}
	document.getElementById("changeFundDateForm-date").value = dateTag.value;
	var date = document.getElementById("changeFundDateForm-date").value;
	var validator = new Validator(changeFundDateSubmit);
    validator.initDateParam(date);
    validator.validate();
}

function changeFundDateSubmit(){
	document.changeFundDateForm.submit();
}

function deleteFundDate(){
    answer = confirm("\u662F\u5426\u522A\u9664" + $("#nowDay").val() + "\u8CC7\u6599 ?");
    if (!answer) {
        return;
    }
    window.location.href = 'deleteFundDate.action';
}