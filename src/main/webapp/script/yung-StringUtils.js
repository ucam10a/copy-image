var com_yung_util_StringUtils = {
    classProp : { name : "com.yung.util.StringUtils"},
    isBlank : function (str) {
        if (str === null || str === '') {
            return true;
        } else {
            return false;
        }
    },
    trim : function (str) {
        return str.replace(/^\s*/, "").replace(/\s*$/, "");
    },
    startsWith : function (str, pattern) {
        if (str.indexOf(pattern) === 0) {
            return true;
        } else {
            return false;
        }
    },
    endsWith : function (str, pattern) {
        var idx = str.indexOf(pattern);
        if (idx === (str.length - pattern.length)) {
            return true;
        } else {
            return false;
        }
    },
    contains : function (str, pattern) {
        if (str.indexOf(pattern) >= 0) {
            return true;
        } else {
            return false;
        }
    },
    replaceAll : function (targetStr, strFind, strReplace) {
        var index = 0;
        while (targetStr.indexOf(strFind, index) != -1) {
            targetStr = targetStr.replace(strFind, strReplace);
            index = targetStr.indexOf(strFind, index);
        }
        return targetStr;
    },
    remove : function (str, pattern) {
        return this.replaceAll(str, pattern, "");
    },
    defaultString : function (str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    },
    toNumber : function (str) {
        return str * 1.0;
    },
    isNumeric : function (str) {
        return jQuery.isNumeric(str);
    },
    parseURL : function (url) {
        var map = {};
        if (url == null) {
            url = decodeURIComponent(window.location.search.substring(1))
        }
        var idx = url.indexOf("?");
        if (idx > 0) {
            url = url.substring(idx + 1, url.length);
        }
        var sURLVariables = url.split('&');
        var sParameterName;
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[1] === undefined) {
                continue;
            } else {
                map[sParameterName[0]] = sParameterName[1];
            }
        }
        return map;
    }
};
$Y.register("com.yung.util.StringUtils", com_yung_util_StringUtils);