var com_yung_util_BasicTemplate = $Class.extend({
    classProp : { name : "com.yung.util.BasicTemplate" },
    condition : null,
    data : null,
    template : null,
    init : function (data, condition) {
        this.setData(data);
        this.template = new com.yung.util.BasicList('string');
        if (condition != null) {
            if (typeof condition == 'function') {
                this.condition = condition;
            } else {
                throw "condition is not function";
            }
        }
        return this;
    },
    setData : function (data) {
        if (data == null) {
            this.data = {};
        } else {
            if (typeof data != 'object') {
                throw "data is not object";
            } else {
                this.data = data;
            }
        }
    },
    add : function (template) {
        if (typeof template != 'string') {
            throw "argument type only allows string";
        }
        this.template.add(template);
    },
    replaceAll : function(targetStr, strFind, strReplace) {
        var index = 0;
        while (targetStr.indexOf(strFind, index) != -1) {
            targetStr = targetStr.replace(strFind, strReplace);
            index = targetStr.indexOf(strFind, index);
        }
        return targetStr;
    },        
    toHtml : function (data) {
        if (data != null) {
            this.setData(data);
        }
        var html = '';
        if (this.condition != null) {
            var valid = this.condition(this.data);
            if (valid == false) {
                return html;
            }
        }
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        for (var key in this.data) {
            var value = this.data[key];
            if (typeof value == 'string' || typeof value == 'number') {
                html = this.replaceAll(html, "{{" + key + "}}", value + '');
            }
        }
        return html;
    }
});

var com_yung_util_Template = com_yung_util_BasicTemplate.extend({
    classProp : { name : "com.yung.util.Template", pattern : "basicTmp" },
    pattern : null,
    basicTmp : null,
    init : function (data, dataArray, basicTmp, condition) {
        this._super(data, null, condition);
        this.setDataArray(dataArray);
        this.setBasicTmp(basicTmp);
        return this;
    },
    setDataArray : function (dataArray) {
    	if (dataArray == null) {
            this.dataArray = [];
        } else {
            if (jQuery.isArray(dataArray) == false) {
                throw "dataArray is not array";
            } else {
                this.dataArray = dataArray;
            }
        }
    },
    setBasicTmp : function (basicTmp) {
        if (basicTmp == null) {
            this.basicTmp = null;
        } else {
            var valid = basicTmp instanceof com_yung_util_BasicTemplate;
            if (valid == false) {
                throw "basicTmp is not instance of com.yung.util.BasicTemplate";
            } else {
                this.basicTmp = basicTmp;
            }
        }
    },
    setPattern : function (pattern) {
        if (typeof pattern != 'string') {
            throw "pattern is not string";
        }
        this.pattern = pattern;
    },
    toHtml : function (data, dataArray) {
        if (data != null) {
            this.setData(data);
        }
        if (dataArray != null) {
            this.setDataArray(dataArray);
        }
        if (this.condition != null) {
            var valid = this.condition(this.data);
            if (valid == false) {
                return "";
            }
        }
        var basicTmpHtml = "";
        if (this.basicTmp != null) {
            for (var i = 0; i < this.dataArray.length; i++) {
                this.basicTmp.setData(this.dataArray[i]);
                basicTmpHtml = basicTmpHtml + this.basicTmp.toHtml();
            }
        }
        var html = '';
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        for (var key in this.data) {
            var value = this.data[key];
            if (typeof value == 'string' || typeof value == 'number') {
                html = this.replaceAll(html, "{{" + key + "}}", value + '');
            }
        }
        if (html == '') {
        	return basicTmpHtml;
        } else {
        	var replacePattern = this.pattern;
            if (replacePattern == null) {
                replacePattern = this.classProp.pattern;
            }
            html = this.replaceAll(html, "{{" + replacePattern + "}}", basicTmpHtml);
        }
        return html;
    }
});

jQuery( document ).ready(function() {
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-BasicTemplate.js requires yung-Collection.js!");
    }
});