var Validator = $Class.extend({
	validateNumner : {
		num1 : null,
		num2 : null,
		num3 : null,
		num4 : null
	},
	validateDate : {
		date1 : null,
		date2 : null,
		date3 : null,
		date4 : null
	},
	next : null,
	init : function (nextFunction) {
		this.validateNumner.num1 = null;
		this.validateNumner.num2 = null;
		this.validateNumner.num3 = null;
		this.validateNumner.num4 = null;
		this.validateDate.date1 = null;
		this.validateDate.date2 = null;
		this.validateDate.date3 = null;
		this.validateDate.date4 = null;
		this.next = nextFunction;
	},
	clear : function (){
		this.validateNumner.num1 = null;
		this.validateNumner.num2 = null;
		this.validateNumner.num3 = null;
		this.validateNumner.num4 = null;
		this.validateDate.date1 = null;
		this.validateDate.date2 = null;
		this.validateDate.date3 = null;
		this.validateDate.date4 = null;
	},
	initNumberParam : function (param1, param2, param3, param4){
		if (param1 != null) this.validateNumner.num1 = param1;
		if (param2 != null) this.validateNumner.num2 = param2;
		if (param3 != null) this.validateNumner.num3 = param3;
		if (param4 != null) this.validateNumner.num4 = param4;
	},
	initDateParam : function (param1, param2, param3, param4){
		if (param1 != null) this.validateDate.date1 = param1;
		if (param2 != null) this.validateDate.date2 = param2;
		if (param3 != null) this.validateDate.date3 = param3;
		if (param4 != null) this.validateDate.date4 = param4;
	},
	validate : function (){
		this.checkNumberFormat(this);
	},
	checkNumberFormat : function (validator){
		if (this.validateNumner.num1 != null) $("#numberValidateForm-num1").val(this.validateNumner.num1);
		if (this.validateNumner.num2 != null) $("#numberValidateForm-num2").val(this.validateNumner.num2);
		if (this.validateNumner.num3 != null) $("#numberValidateForm-num3").val(this.validateNumner.num3);
		if (this.validateNumner.num4 != null) $("#numberValidateForm-num4").val(this.validateNumner.num4);
		var ajaxhttp = new com.yung.util.AjaxHttp('numberValidate', 'numberValidateForm', function callback(response, self)  
	    {
	        var errormsg = response.errormsg;
	        if (errormsg == '') {
	        	validator.checkDateFormat(validator);
	        } else {
	            alert(errormsg);
	        }
	    });
	    ajaxhttp.send();
	},
	checkDateFormat : function (validator){
		if (this.validateDate.date1 != null) $("#dateValidateForm-date1").val(this.validateDate.date1);
		if (this.validateDate.date2 != null) $("#dateValidateForm-date2").val(this.validateDate.date2);
		if (this.validateDate.date3 != null) $("#dateValidateForm-date3").val(this.validateDate.date3);
		if (this.validateDate.date4 != null) $("#dateValidateForm-date4").val(this.validateDate.date4);
		var ajaxhttp = new com.yung.util.AjaxHttp('dateValidate', 'dateValidateForm', function callback(response, self)  
	    {
	        var errormsg = response.errormsg;
	        if (errormsg == '') {
	        	validator.next();
	        } else {
	            alert(errormsg);
	        }
	    });
	    ajaxhttp.send();
	}
		
});