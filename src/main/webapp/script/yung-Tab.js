    var com_yung_util_Tab = $Class.extend({
        classProp : { name : "com.yung.util.Tab" },
        divId : null,
        titles : null,
        color : null,
        width : null,
        span : null,
        srcContent : null,
        init : function (divId, titles, tabId, color, width, span) {
            if (divId == null) {
                throw "Please specify divId";
            }
            this.divId = divId;
            if (titles == null) {
                throw "Please specify titles";
            }
            if (jQuery.isArray(titles) == false) {
                throw "titles has to be array";
            }
            this.titles = titles;
            if (color == null) {
                throw "Please specify color";
            }
            this.color = color;
            if (tabId == null) {
                throw "Please specify tabId";
            }
            this.tabId = tabId;
            if (width == null) {
                this.width = 120;
            } else {
                this.width = width;
            }
            if (span == null) {
                this.span = 30;
            } else {
                this.span = span;
            }
            this.srcContent = new com_yung_util_BasicList("string");
            this.createTab();
            for (var i = 0; i < this.srcContent.size(); i++) {
                this.setTabContent(i, this.srcContent.get(i));
            }
            this.showTab(0);
            return this;
        },
        createTab : function () {
            this.getSourceTabContent();
            var headBasicTmp = new com.yung.util.BasicTemplate();
            headBasicTmp.add('<td width="{{width}}">');
            headBasicTmp.add('    <div id="{{tabId}}Tab{{idx}}Show" style="display:none; border-color: {{tabColor}}; background-color: {{tabColor}}; color: white; white-space: nowrap; text-align: center; border-style: solid; border-color: {{tabColor}}; border-width: 2px 1px 0px 1px; padding-top: 5px; padding-bottom: 5px; cursor: pointer;" >');
            headBasicTmp.add('        <span>{{title}}</span>');
            headBasicTmp.add('    </div>');
            headBasicTmp.add('    <div id="{{tabId}}Tab{{idx}}Wait" style="display:none; border-color: {{tabColor}}; background-color: white; color: {{tabColor}}; white-space: nowrap; text-align: center; border-style: solid; border-color: {{tabColor}}; border-width: 2px 2px 0px 2px; padding-top: 5px; padding-bottom: 5px; cursor: pointer;" onclick="com_yung_util_Tab.display({{length}}, \'{{tabId}}\', {{idx}}, \'{{tabColor}}\', \'{{tabId}}Callback\');">');
            headBasicTmp.add('        <span>{{title}}</span>');
            headBasicTmp.add('    </div>');
            headBasicTmp.add('    </div>');
            headBasicTmp.add('</td>');
            headBasicTmp.add('<td width="{{span}}" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
            var headTmp = new com.yung.util.Template();
            headTmp.setBasicTmp(headBasicTmp);
            var contentBasicTmp = new com.yung.util.BasicTemplate();
            contentBasicTmp.add('<div id="{{tabId}}TabContent{{idx}}" style="display:none; border-top: 1px solid {{tabColor}}; padding-left: 10px; padding-right: 10px;">');
            contentBasicTmp.add('</div>');
            var contentTmp = new com.yung.util.Template();
            contentTmp.setBasicTmp(contentBasicTmp);
            var dataArray = this.getDataArray();
            var mainTmp = new com.yung.util.BasicTemplate();
            mainTmp.add('<table border="0" cellpadding="0" cellspacing="0" width="100%">');
            mainTmp.add('    <tr>');
            mainTmp.add('        <td width="15" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
            mainTmp.add('        {{headTmp}}');
            mainTmp.add('        <td width="*" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
            mainTmp.add('    </tr>');
            mainTmp.add('    <tr>');
            mainTmp.add('        <td colspan="{{colspan}}">');
            mainTmp.add('            {{contentTmp}}');
            mainTmp.add('        </td>');
            mainTmp.add('    </tr>');
            mainTmp.add('</table>');
            var mainData = {};
            mainData["colspan"] = (this.titles.length * 2) + 2;
            mainData["tabColor"] = this.color;
            mainData["headTmp"] = headTmp.toHtml(null, dataArray);
            mainData["contentTmp"] = contentTmp.toHtml(null, dataArray);
            jQuery("#" + this.divId).html(mainTmp.toHtml(mainData));
        },
        getDataArray : function () {
            var dataArray = [];
            for (var i = 0; i < this.titles.length; i++) {
                var data = {};
                data["title"] = this.titles[i];
                data["tabId"] = this.tabId;
                data["tabColor"] = this.color;
                data["length"] = this.titles.length;
                data["idx"] = i + "";
                data["width"] = this.width + "";
                data["span"] = this.span + "";
                dataArray.push(data);
            }
            return dataArray;
        },
        showTab : function (idx) {
            com_yung_util_Tab.display(this.titles.length, this.tabId, idx, this.color);
        },
        getSourceTabContent : function () {
            var self = this;
            jQuery("#" + this.divId + " .yungTabContent").each(function(index) {
                var html = jQuery(this).html();
                self.srcContent.add(html);
            });
        },
        setTabContent : function (idx, html) {
            jQuery("#" + this.tabId + "TabContent" + idx).html(html);
        },
        setTabTitle : function (idx, title) {
        	jQuery("#" + this.tabId + "Tab" + idx + "Show").html("<span>" + title + "</span>");
        	jQuery("#" + this.tabId + "Tab" + idx + "Wait").html("<span>" + title + "</span>");
        }
    });
    com_yung_util_Tab.display = function(totalTabs, tabId, idx, color, callback) {
        for ( var i = 0; i < totalTabs; i++) {
            if (idx == i) {
                jQuery("#" + tabId + "Tab" + i + "Show").css("display", "block");
                jQuery("#" + tabId + "Tab" + i + "Show").css("background-color", color);
                jQuery("#" + tabId + "Tab" + i + "Show").css("color", "white");
                jQuery("#" + tabId + "Tab" + i + "Wait").css("display", "none");
                jQuery("#" + tabId + "Tab" + i + "Wait").css("background-color", "");
                jQuery("#" + tabId + "Tab" + i + "Wait").css("color", color);
                jQuery("#" + tabId + "TabContent" + i).css("display", "block");
            } else {
                jQuery("#" + tabId + "Tab" + i + "Show").css("display", "none");
                jQuery("#" + tabId + "Tab" + i + "Show").css("background-color", "");
                jQuery("#" + tabId + "Tab" + i + "Show").css("color", color);
                jQuery("#" + tabId + "Tab" + i + "Wait").css("display", "block");
                jQuery("#" + tabId + "Tab" + i + "Wait").css("background-color", "");
                jQuery("#" + tabId + "Tab" + i + "Wait").css("color", color);
                jQuery("#" + tabId + "TabContent" + i).css("display", "none");
            }
        }
        if (typeof window[callback] == 'function') {
            window[callback](idx);
        }
    }
    jQuery( document ).ready(function() {
        if (typeof com_yung_util_BasicTemplate == 'undefined') {
            alert("yung-Tab.js requires yung-BasicTemplate.js!");
        }
    });