    var com_yung_util_Movable = $Class.extend({
        classProp : { name : "com.yung.util.Movable" },
        titleId : null,
        divId : null,
        init: function(titleId, divId){
            this.titleId = titleId;
            this.divId = divId;
            this.draggable(document.getElementById(divId));
            return this;
        },
        addListener : function (element, type, callback, capture) {
            if (element.addEventListener) {
                element.addEventListener(type, callback, capture);
            } else {
                element.attachEvent("on" + type, callback);
            }
        },
        draggable : function (element) {
            var dragging = null;
            this.addListener(element, "mousedown", function (e) {
                var evt = window.event || e;
                dragging = {
                    mouseX: evt.clientX,
                    mouseY: evt.clientY,
                    startX: parseInt(element.style.left),
                    startY: parseInt(element.style.top)
                };
                if (element.setCapture) element.setCapture();
            });
            this.addListener(element, "losecapture", function () {
                dragging = null;
            });
            this.addListener(document, "mouseup", function () {
                dragging = null;
                if (document.releaseCapture) document.releaseCapture();
            }, true);
            var dragTarget = element.setCapture ? element : document;
            this.addListener(dragTarget, "mousemove", function (e) {
                if (!dragging) return;
                var evt = window.event || e;
                var top = dragging.startY + (evt.clientY - dragging.mouseY);
                var left = dragging.startX + (evt.clientX - dragging.mouseX);
                element.style.top = (Math.max(0, top)) + "px";
                element.style.left = (Math.max(0, left)) + "px";
            }, true);
        }
    });
    