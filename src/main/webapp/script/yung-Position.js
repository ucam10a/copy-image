var com_yung_util_Position = $Class.extend({
    classProp : { name : "com.yung.util.Position" },
    element : null,
    init : function (element) {
        if (typeof element.get == 'function') {
            this.element = element;
        } else {
            this.element = jQuery(element);
        }
        return this;
    },
    getBaseTop : function () {
    	var doc = document.documentElement;
        var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
        return top;
    },
    getBaseLeft : function () {
    	var doc = document.documentElement;
    	var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
    	return left;
    },
    getBotLeftPosition : function (offsetX, offsetY) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseTop = this.getBaseTop();
        var scrollTop = jQuery('body').scrollTop();
        var baseLeft = this.getBaseLeft();
        var scrollLeft = jQuery('body').scrollLeft();
        var rect = this.element.get(0).getBoundingClientRect();
        var pos = {};
        pos.left = rect.left + scrollLeft + baseLeft + offsetX;
        pos.top = rect.top + scrollTop + baseTop + this.element.height() + offsetY;
        return pos;
    },
    getTopLeftPosition : function (offsetX, offsetY) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseTop = this.getBaseTop();
        var scrollTop = jQuery('body').scrollTop();
        var baseLeft = this.getBaseLeft();
        var scrollLeft = jQuery('body').scrollLeft();
        var rect = this.element.get(0).getBoundingClientRect();
        var pos = {};
        pos.left = rect.left + scrollLeft + baseLeft + offsetX;
        pos.top = rect.top + scrollTop + baseTop + offsetY;
        return pos;
    },
    getTopRightPosition : function (offsetX, offsetY) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseTop = this.getBaseTop();
        var scrollTop = jQuery('body').scrollTop();
        var baseLeft = this.getBaseLeft();
        var scrollLeft = jQuery('body').scrollLeft();
        var rect = this.element.get(0).getBoundingClientRect();
        var pos = {};
        pos.left = rect.left + scrollLeft + baseLeft + this.element.width() + offsetX;
        pos.top = rect.top + scrollTop + baseTop + offsetY;
        return pos;
    },
    getBotRightPosition : function (offsetX, offsetY) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseTop = this.getBaseTop();
        var scrollTop = jQuery('body').scrollTop();
        var baseLeft = this.getBaseLeft();
        var scrollLeft = jQuery('body').scrollLeft();
        var rect = this.element.get(0).getBoundingClientRect();
        var pos = {};
        pos.left = rect.left + scrollLeft + baseLeft + this.element.width() + offsetX;
        pos.top = rect.top + scrollTop + baseTop + this.element.height() + offsetY;
        return pos;
    },
    getCenterPosition : function (width, height) {
        if (width == null) {
            width = 0;
        }
        if (height == null) {
            height = 0;
        }
        var windowHeight = jQuery(document).height();
        var windowWidth = jQuery(document).width();
        var pos = {};
        pos["left"] = (windowWidth - width) / 2;
        pos["top"] = (windowWidth - height) / 2;
        return pos;
    }
});