    var com_yung_util_Entry = $Class.extend({
        classProp : { name : "com.yung.util.Entry"},
        key : null,
        value : null,
        init : function(key, value) {
            this.key = key;
            this.value = value;
        },
        getKey : function () {
            return this.key;
        },
        getValue : function () {
            return this.value;
        },
        setValue : function (value) {
            this.value = value;
        }
    });    
    
    var com_yung_util_Map = $Class.extend({
        classProp : { name : "com.yung.util.Map"},
        keyType : null,
        valueType : null,
        keyArray : null,
        obj : null,
        debug : null,
        init : function(keyType, valueType, debug){
            if (keyType == null || valueType == null) {
                throw "com.yung.util.Map key type and value type can not be null!";
            }
            if (typeof keyType == "string") {
                keyType = keyType.toLowerCase();
            }
            if (keyType != 'string' && keyType != 'number') {
                throw "com.yung.util.Map key type can only be string or number!";
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.keyType = keyType;
            this.valueType = valueType;
            this.obj = {};
            this.keyArray = [];
            return this;
        },
        clear : function () {
            this.obj = {};
            this.keyArray = [];
        },
        containsKey : function (key) {
            if (key == null) {
                return false;
            }
            if (typeof key != this.keyType) {
                return false;
            }
            if (jQuery.inArray(key, this.keyArray) < 0) {
                return false;
            } else {
                return true;
            }
        },
        containsValue : function (value) {
            if (value == null) {
                return false;
            }
            if (this.validValueType(value) == false) {
                return false;
            }
            for (var key in this.obj) {
                if (this.obj[key] === value) {
                    return true;
                }
            }
            return false;
        },
        get : function (key) {
            if (key == null) {
                return null;
            }
            if (typeof key != this.keyType) {
                return null;
            }
            return this.obj[key];
        },
        put : function (key, value) {
            if (typeof key != this.keyType) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(value) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else if (typeof this.valueType == "function") {
                    throw "value type is not " + new this.valueType().classProp.name;
                } else {
                    throw "value type is not compatible";
                }
            }
            if (jQuery.inArray(key, this.keyArray) < 0) {
                this.keyArray.push(key);
                this.obj[key] = value;
            } else {
                this.obj[key] = value;
            }
        },
        putAll : function (map) {
            if (map == null) {
                return;
            } else {
                if (map.keyType != this.keyType) {
                    throw "key type is not " + this.keyType;
                }
                if (map.valueType != this.valueType) {
                    if (typeof this.valueType == "string") {
                        throw "value type is not string";
                    } else if (typeof this.valueType == "number") {
                        throw "value type is not number";
                    } else if (typeof this.valueType == "object") {
                        throw "value type is not object";
                    } else if (typeof this.valueType == "function") {
                        throw "value type is not " + new this.valueType().classProp.name;
                    } else {
                        throw "value type is not compatible";
                    }
                }
                var entryArray = map.entryArray()
                for (var i = 0; i < entryArray.length; i++) {
                    var entry = entryArray[i];
                    this.put(entry.getKey(), entry.getValue());
                }
            }
        },
        remove : function (key) {
            if (key == null) {
                return null;
            }
            if (typeof key != this.keyType) {
                return null;
            }
            var index = jQuery.inArray(key, this.keyArray);
            if (index < 0) {
                return null;
            } else {
                var value = this.obj[this.keyArray[index]]
                this.keyArray.splice(index, 1);
                this.obj[key] = null;
                return value;                
            }
        },
        size : function () {
            return this.keyArray.length;
        },
        isEmpty : function () {
            if (this.size() == 0) {
                return true;
            } else {
                return false;
            }
        },
        entrySet : function () {
            var set = new com.yung.util.ObjectHashSet(com.yung.util.Entry);
            for (var i = 0; i < this.keyArray.length; i++) {
                var entry = new com.yung.util.Entry(this.keyArray[i], this.obj[this.keyArray[i]]);
                set.add(entry);
            }
            return set;
        },
        entryArray : function () {
            var array = [];
            for (var i = 0; i < this.keyArray.length; i++) {
                var entry = new com.yung.util.Entry(this.keyArray[i], this.obj[this.keyArray[i]]);
                array.push(entry);
            }
            return array;
        },
        getKeyArray : function () {
            return this.keyArray;
        },
        keySet : function () {
            var set = new com.yung.util.Set(this.keyType);
            for (var i = 0; i < this.keyArray.length; i++) {
                set.add(this.keyArray[i]);
            }
            return set;
        },
        values : function () {
            var list = new com.yung.util.List(this.valueType);
            var entryArray = this.entryArray();
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                list.add(entry.getValue());
            }
            return list;
        },
        toString : function () {
            var ret = this.classProp.name + "\n";
            for (var i = 0; i < this.keyArray.length; i++) {
                var val = this.obj[this.keyArray[i]];
                var valStr = "";
                if (typeof val.toString == 'function') {
                    valStr = val.toString();
                } else {
                    valStr = JSON.stringify(this.obj[this.keyArray[i]])
                }
                if (this.keyType == 'string') {
                    ret = ret + "'" + this.keyArray[i] + "': " + valStr + " \n";
                } else if (this.keyType == 'number') {
                    ret = ret + this.keyArray[i] + ": " + valStr + " \n";
                }
            }
            return ret;
        },
        clone : function () {
            var ret = new com.yung.util.Map(this.keyType, this.valueType);
            ret.putAll(this);
            return ret;
        },
        validValueType : function (value) {
            if (typeof value == this.valueType) {
                return true;
            }
            var ret = value instanceof this.valueType;
            return ret;
        }
    });
    
    var com_yung_util_ValueHashMap = com_yung_util_Map.extend({
        classProp : { name : "com.yung.util.ValueHashMap"},
        keyType : null,
        valueType : null,
        keyArray : null,
        valueInstance : null,
        valueInstanceName : null,
        obj : null,
        debug : null,
        init : function(keyType, valueType, debug){
            if (keyType == null || valueType == null) {
                throw "com.yung.util.ValueHashMap key type and value type can not be null!";
            }
            if (typeof keyType == "string") {
                keyType = keyType.toLowerCase();
            }
            if (keyType != 'string' && keyType != 'number') {
                throw "com.yung.util.ValueHashMap key type can only be string or number!";
            }
            if (typeof valueType != 'function') {
                throw "value type is not function";
            }
            this.valueInstance = new valueType();
            if (this.valueInstance.classProp != null && this.valueInstance.classProp.name != null) {
                this.valueInstanceName = this.valueInstance.classProp.name;
            } else {
                this.valueInstanceName = this.valueInstance.constructor.name;
            }
            if (typeof this.valueInstance.hashCode != 'function') {
                throw "Please implement hashCode function in " + this.valueInstanceName;
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.keyType = keyType;
            this.valueType = valueType;
            this.obj = {};
            this.keyArray = [];
            return this;
        },
        containsValue : function (value) {
            if (value == null) {
                return false;
            }
            if (this.validValueType(value) == false) {
                return false;
            }
            for (var key in this.obj) {
                this.obj[key].hashCode()
                if (this.obj[key].hashCode() === value.hashCode()) {
                    return true;
                }
            }
            return false;
        },
        clone : function () {
            var ret = new com.yung.util.ValueHashMap(this.keyType, this.valueType);
            ret.putAll(this);
            return ret;
        }
    });
    
    var com_yung_util_TreeMap = com_yung_util_Map.extend({
        classProp : { name : "com.yung.util.TreeMap"},
        keyType : null,
        valueType : null,
        keyArray : null,
        obj : null,
        reverse : null,
        init : function(keyType, valueType, reverse){
            if (keyType == null || valueType == null) {
                throw "com.yung.util.TreeMap key type and value type can not be null!";
            }
            if (typeof keyType == "string") {
                keyType = keyType.toLowerCase();
            }
            if (keyType != 'string' && keyType != 'number') {
                throw "com.yung.util.TreeMap key type can only be string or number!";
            }
            if (reverse === true) {
                this.reverse = true;
            } else {
                this.reverse = false;
            }
            this.keyType = keyType;
            this.valueType = valueType;
            this.obj = {};
            this.keyArray = [];
            return this;
        },
        put : function (key, value) {
            this._super(key, value);
            if (this.reverse == false) {
                if (this.keyType == 'string') {
                    this.keyArray.sort();
                } else if (this.keyType == 'number') {
                    this.keyArray.sort(function(a, b){return a - b});
                }
            } else {
                if (this.keyType == 'string') {
                    this.keyArray.sort();
                    this.keyArray.reverse();
                } else if (this.keyType == 'number') {
                    this.keyArray.sort(function(a, b){return b - a});
                }
            }
        },
        clone : function () {
            var ret = new com.yung.util.TreeMap(this.keyType, this.valueType);
            ret.putAll(this);
            return ret;
        }
    });
    jQuery( document ).ready(function() {
        if (typeof com_yung_util_Collection == 'undefined') {
            alert("yung-Map.js requires yung-Collection.js!");
        }
    });