    var com_yung_util_FloatDiv = $Class.extend({
        classProp : { name : "com.yung.util.FloatDiv" },
        divId : null,
        init : function (divId) {
            if (yung_global_var["com_yung_util_FloatDiv" + divId] != null) {
                throw "Please use com.yung.util.FloatDiv.instance() to create instance!";
            }
            this.divId = divId;
            com_yung_util_FloatDiv.instance(this);
            this.createFloatDiv();
            return this;
        },
        bindCloseEvent : function (close, element, event) {
            if (element == null) {
                element = jQuery("body");
            }
            if (event == null) {
                event = "click";
            }
            if (typeof close == 'function') {
                element.bind( event, function() {
                    close();
                });    
            }
        },
        createFloatDiv : function () {
            var floatDiv = jQuery("#" + this.divId)[0];
            if (floatDiv == null) {
                var obj = {};
                obj["divId"] = this.divId;
                var template = new com.yung.util.BasicTemplate(obj);
                template.add('<div id="yung-FloatDiv-{{divId}}" class="float-border" style="position: absolute; left: 0px; top: 0px; margin: 3px; display: none; background: white; box-shadow: 5px 5px 5px #d7d7d7; width:300px; z-index: 5000;">');
                template.add('    <div id="yung-FloatDiv-container-{{divId}}" style="display: block; width: 100%; height: 100%;">');
                template.add('        <div id="yung-FloatDiv-content-{{divId}}" style="position: absolute; margin: 3px; height: 100%; width: 100%;" ></div>');
                template.add('        <div style="position: absolute; right: 2px; top: 0px; cursor: pointer;"><span style="color: red;" title="close" onclick="com_yung_util_FloatDiv.instance(\'{{divId}}\').showFloatDiv(false);" style="cursor: pointer;" >&#10006;</span></div>');
                template.add('    </div>');
                template.add('</div>');
                var html = template.toHtml();
                jQuery("body").append(html);
            }
        },
        setFloatContent : function (html) {
            jQuery("yung-FloatDiv-content-" + this.divId).html(html);
        },
        showFloatContent : function () {
            jQuery("#yung-FloatDiv-container-" + this.divId).css("display", "block");
        },
        showFloatDiv : function (show, divId) {
            if (divId == null) {
                divId = this.divId;
            }
            if (show == false) {
                var display = jQuery("#yung-FloatDiv-" + divId).css("display");
                if (display != 'none') {
                    jQuery("#yung-FloatDiv-" + divId).css("display", "none");
                }
                return; 
            }
            var display = jQuery("#yung-FloatDiv-" + divId).css("display");
            if (display != 'block') {
                jQuery("#yung-FloatDiv-" + divId).css("display", "block");
            }
        },
        closeFloatDiv : function (callback) {
            this.showFloatDiv(false);
            if (typeof callback === "function") {
                callback();
            }   
        },
        openFloatDiv : function (html, pos, width, height) {
            if (width == null) {
                width = 500;
            }
            if (height == null) {
                height = 300;
            }
            if (pos == null) {
                throw "please specify position!";
            }
            var windowWidth = jQuery(document).width();
            if (pos.left < 0) pos.left = 0; 
            if ((pos.left + width) > windowWidth) pos.left = windowWidth - width;
            var top = pos.top + "px";
            var left = pos.left + "px";
            jQuery("#yung-FloatDiv-" + this.divId).css("width", width);
            jQuery("#yung-FloatDiv-" + this.divId).css("height", height);
            jQuery("#yung-FloatDiv-" + this.divId).css("left", left);
            jQuery("#yung-FloatDiv-" + this.divId).css("top", top);
            jQuery("#yung-FloatDiv-content-" + this.divId).html(html);
            // use setTimeout to prevent body click event conflict
            var show = this.showFloatDiv;
            var divId = this.divId;
            setTimeout(function(){ show(true, divId); }, 300);
        }
    });
    com_yung_util_FloatDiv.instance = function(floatDiv) {
        if (floatDiv != null) {
            if (floatDiv instanceof com_yung_util_FloatDiv) {
                yung_global_var["com_yung_util_FloatDiv-" + floatDiv.divId] = floatDiv;
            }
            if (typeof floatDiv == 'string') {
                var instance = yung_global_var["com_yung_util_FloatDiv-" + floatDiv];
                if (instance == null) {
                    instance = new com_yung_util_FloatDiv(floatDiv);
                }
                return instance;
            }
        }
    }
    jQuery( document ).ready(function() {
        if (typeof com_yung_util_BasicTemplate == 'undefined') {
            alert("yung-FloatDiv.js requires yung-BasicTemplate.js!");
        }
    });