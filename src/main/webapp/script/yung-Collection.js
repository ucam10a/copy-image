    var com_yung_util_Collection = $Class.extend({
        classProp : { name : "com.yung.util.Collection"},
        type : null,
        array : null,
        debug : null,
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.Collection type can not be null!";
            }
            if (typeof type == "string") {
                type = type.toLowerCase();
            }
            if (type != 'string' && type != 'number') {
                throw "com.yung.util.Collection type can only be string or number!";
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        log : function (msg) {
            if (window['console'] != null) {
                if (this.debug) console.log(msg);
            }
        },
        clear : function () {
            this.array = [];
        },
        addByArray : function (elementArray) {
            if (elementArray != null) {
                if (jQuery.isArray(elementArray)) {
                    for (var i = 0; i < elementArray.length; i++) {
                        this.add(elementArray[i]);
                    }
                } else {
                    throw "argument type is not array";
                }
            }
        },
        contains : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                return false;
            }
            if (jQuery.inArray(element, this.array) < 0) {
                return false;
            } else {
                return true;
            }
        },
        isEmpty : function () {
            if (this.array.length == 0) {
                return true;
            } else {
                return false;
            }
        },
        iterator : function () {
            return this.array;
        },
        size : function () {
            return this.array.length;
        },
        toArray : function () {
            return this.array;
        },
        toString : function () {
            var ret = "";
            if (this.array.length > 0 && (this.type == 'string' || this.type == 'number')) {
                ret = ret + "[";
            }
            for (var i = 0; i < this.array.length; i++) {
                
                if (this.type == 'string') {
                    ret = ret + "'" + this.array[i] + "', ";
                } else if (this.type == 'number') {
                    ret = ret + this.array[i] + ", ";
                } else {
                    if (i == 0) ret = ret + "\n";
                    if (jQuery.isArray(this.array[i])) {
                        ret = ret + JSON.stringify(this.array[i]) + ",\n"; 
                    } else if (typeof this.array[i].toString == "function") {
                        ret = ret + this.array[i].toString() + ",\n"; 
                    } else {
                        ret = ret + JSON.stringify(this.array[i]) + ",\n"; 
                    }
                }
            }
            if (ret.length > 0) {
                ret = ret.substring(0, ret.length - 2);
            }
            if (this.array.length > 0 && (this.type == 'string' || this.type == 'number')) {
                ret = ret + "]";
            }
            return this.classProp.name + ret;
        },    
        sort : function (reverse) {
            if (reverse == null || reverse == false) {
                reverse = false;
            } else {
                reverse = true;
            }
            if (reverse == false) {
                if (this.type == 'string') {
                    this.array.sort();
                } else if (this.type == 'number') {
                    this.array.sort(function(a, b){return a - b});
                } else if (typeof this.type == 'function') {
                    var instance = new this.type();
                    if (typeof instance.compareTo == 'function') {
                        this.array.sort(function(a, b){return a.compareTo(b);});
                    }
                }
            } else {
                if (this.type == 'string') {
                    this.array.sort();
                    this.array.reverse();
                } else if (this.type == 'number') {
                    this.array.sort(function(a, b){return b - a});
                } else if (typeof this.type == 'function') {
                    var instance = new this.type();
                    if (typeof instance.compareTo == 'function') {
                        this.array.sort(function(a, b){return b.compareTo(a);});
                    }
                }
            }
        },
        isCollection : function (obj) {
            var ret = obj instanceof com.yung.util.Collection;
            return ret;
        },
        validType : function (element) {
            if (typeof element == this.type) {
                return true;
            }
            var ret = element instanceof this.type;
            return ret;
        }
    });
    
    var com_yung_util_BasicSet = com_yung_util_Collection.extend({
        classProp : { name : "com.yung.util.BasicSet"},
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.BasicSet type can not be null!";
            }
            if (typeof type == "string") {
                type = type.toLowerCase();
            }
            if (type != 'string' && type != 'number') {
                throw "com.yung.util.BasicSet type can only be string or number!";
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        add : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                throw "argument type must be " + this.type;
            }
            if (jQuery.inArray(element, this.array) < 0) {
                this.array.push(element);
            }
        },
        addAll : function (set) {
            if (set == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(set) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (set.type != this.type) {
                throw "argument type must be " + this.type;
            }
            for (var i = 0; i < set.array.length; i++) {
                this.add(set.array[i]);
            }
            return true;
        },
        containsAll : function (set) {
            if (set == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(set) == false) {
                return false;
            }
            if (this.size() > set.size()) {
                return false;
            }
            for (var i = 0; i < set.array.length; i++) {
                if (this.contains(set.array[i]) == false) {
                    return false;
                }
            }
            return true;
        },
        equals : function (set) {
            if (set == null) {
                return false;
            }
            if (this.isCollection(set) == false) {
                return false;
            }
            if (set.array.length != this.array.length) {
                return false;
            }
            if (this.containsAll(set)) {
                return true;
            } else {
                return false;
            }
        },
        remove : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                return false;
            }
            var index = jQuery.inArray(element, this.array);
            if (index < 0) {
                return false;
            } else {
                if (index !== -1) {
                    this.array.splice(index, 1);
                    return true;
                } else {
                    return false;
                }
            }
        },
        removeAll : function (set) {
            if (set == null) {
                return false;
            }
            if (this.isCollection(set) == false) {
                return false;
            }
            for (var i = 0; i < set.array.length; i++) {
                this.remove(set.array[i]);
            }
            return true;
        },
        retainAll : function (set) {
            if (set == null) {
                return false;
            }
            if (this.isCollection(set) == false) {
                return false;
            }
            var tempArray = [];
            for (var i = 0; i < this.array.length; i++) {
                if (set.contains(this.array[i])) {
                    tempArray.push(this.array[i]);
                }
            }
            this.array = tempArray;
            return true;
        }
    });
    
    var com_yung_util_ArraySet = com_yung_util_BasicSet.extend({
        classProp : { name : "com.yung.util.ArraySet"},
        hashArray : null,
        init : function(debug){
            this.type = Array;
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.array = [];
            this.hashArray = [];
            return this;
        },
        add : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                throw "argument type must be Array";
            }
            var json = JSON.stringify(element);
            var hash = MD5Util.calc(json);
            if (jQuery.inArray(hash, this.hashArray) < 0) {
                this.hashArray.push(hash);
                this.array.push(element);
            }
        },
        addAll : function (set) {
            if (set == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(set) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (set.type != this.type) {
                throw "argument type must be Array";
            }
            for (var i = 0; i < set.array.length; i++) {
                this.add(set.array[i]);
            }
            return true;
        },
        contains : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                return false;
            }
            var json = JSON.stringify(element);
            var hashCode = MD5Util.calc(json);
            if (jQuery.inArray(hashCode, this.hashArray) < 0) {
                return false;
            } else {
                return true;
            }
        },
        remove : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                return false;
            }
            var json = JSON.stringify(element);
            var hashCode = MD5Util.calc(json);
            var index = jQuery.inArray(hashCode, this.hashArray);
            if (index < 0) {
                return false;
            } else {
                this.hashArray.splice(index, 1);
                this.array.splice(index, 1);
                return true;                
            }
        },
        retainAll : function (set) {
            if (set == null) {
                return false;
            }
            if (this.isCollection(set) == false) {
                return false;
            }
            var tempHashArray = [];
            var tempArray = [];
            for (var i = 0; i < this.array.length; i++) {
                if (set.contains(this.array[i])) {
                    tempHashArray.push(this.hashArray[i]);
                    tempArray.push(this.array[i]);
                }
            }
            this.hashArray = tempHashArray;
            this.array = tempArray;
            return true;
        }
    });
    
    var com_yung_util_RawObjectSet = com_yung_util_BasicSet.extend({
        classProp : { name : "com.yung.util.RawObjectSet"},
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.RawObjectSet type can not be null!";
            }
            if (typeof type == "string") {
                type = type.toLowerCase();
            }
            if (type != 'object') {
                throw "com.yung.util.RawObjectSet type can only be string or number!";
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        add : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != 'object') {
                throw "argument type must be object";
            }
            if (jQuery.inArray(element, this.array) < 0) {
                this.array.push(element);
            }
        },
        addAll : function (set) {
            if (set == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(set) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (set.type != this.type) {
                throw "argument type must be object";
            }
            for (var i = 0; i < set.array.length; i++) {
                this.add(set.array[i]);
            }
            return true;
        }
    });
    
    var com_yung_util_ObjectSet = com_yung_util_BasicSet.extend({
        classProp : { name : "com.yung.util.ObjectSet"},
        instance : null,
        instanceName : null,
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.ObjectSet type can not be null!";
            }
            if (typeof type != 'function') {
                throw "com.yung.util.ObjectSet type can only be function!";
            }
            this.instance = new type();
            if (this.instance.classProp != null && this.instance.classProp.name != null) {
                this.instanceName = this.instance.classProp.name;
            } else {
                this.instanceName = this.instance.constructor.name;
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        add : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                throw "argument type must be function";
            }
            if (jQuery.inArray(element, this.array) < 0) {
                this.array.push(element);
            }
        },
        addAll : function (set) {
            if (set == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(set) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (set.type != this.type) {
                throw "argument type must be function";
            }
            for (var i = 0; i < set.array.length; i++) {
                this.add(set.array[i]);
            }
            return true;
        }
    });
    
    var com_yung_util_TreeSet = com_yung_util_BasicSet.extend({
        classProp : { name : "com.yung.util.TreeSet"},
        reverse : null,
        init : function(type, reverse){
            if (type == null) {
                throw "com.yung.util.TreeSet type can not be null!";
            }
            if (typeof type == "string") {
                type = type.toLowerCase();
            }
            if (type != 'string' && type != 'number') {
                throw "com.yung.util.TreeSet type can only be string or number!";
            }
            if (reverse === true) {
                this.reverse = true;
            } else {
                this.reverse = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        add : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                throw "argument type must be " + this.type;
            }
            if (jQuery.inArray(element, this.array) < 0) {
                this.array.push(element);
            }
            this.sort(this.reverse);
        }
    });
    
    var com_yung_util_ObjectHashSet = com_yung_util_BasicSet.extend({
        classProp : { name : "com.yung.util.ObjectHashSet"},
        instance : null,
        instanceName : null,
        hashArray : null,
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.ObjectHashSet type can not be null!";
            }
            if (typeof type != 'function') {
                throw "com.yung.util.ObjectHashSet type can only be function!";
            }
            this.instance = new type();
            if (this.instance.classProp != null && this.instance.classProp.name != null) {
                this.instanceName = this.instance.classProp.name;
            } else {
                this.instanceName = this.instance.constructor.name;
            }
            if (typeof this.instance.hashCode != 'function') {
                throw "Please implement hashCode function in " + this.instanceName;
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.hashArray = [];
            this.array = [];
            return this;
        },
        add : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                throw "argument type is not " + this.instanceName;
            }
            var hashCode = element.hashCode();
            if (jQuery.inArray(hashCode, this.hashArray) < 0) {
                this.hashArray.push(hashCode);
                this.array.push(element);
            }
        },
        addAll : function (set) {
            if (set == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(set) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (set.type != this.type) {
                throw "argument type is not " + this.instanceName;
            }
            for (var i = 0; i < set.array.length; i++) {
                this.add(set.array[i]);
            }
            return true;
        },
        contains : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                return false;
            }
            if (jQuery.inArray(element.hashCode(), this.hashArray) < 0) {
                return false;
            } else {
                return true;
            }
        },
        remove : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                return false;
            }
            var hashCode = element.hashCode();
            var index = jQuery.inArray(hashCode, this.hashArray);
            if (index < 0) {
                return false;
            } else {
                this.hashArray.splice(index, 1);
                this.array.splice(index, 1);
                return true;
            }
        },
        retainAll : function (set) {
            if (set == null) {
                return false;
            }
            if (this.isCollection(set) == false) {
                return false;
            }
            var tempHashArray = [];
            var tempArray = [];
            for (var i = 0; i < this.array.length; i++) {
                if (set.contains(this.array[i])) {
                    tempHashArray.push(this.hashArray[i]);
                    tempArray.push(this.array[i]);
                }
            }
            this.hashArray = tempHashArray;
            this.array = tempArray;
            return true;
        }
    });
    
    var com_yung_util_TreeHashSet = com_yung_util_ObjectHashSet.extend({
        classProp : { name : "com.yung.util.TreeHashSet"},
        reverse : null,
        instance : null,
        instanceName : null,
        instanceName : null,
        hashArray : null,
        array : null,
        init : function(type, reverse){
            if (type == null) {
                throw "com.yung.util.TreeSet type can not be null!";
            }
            if (typeof type != "function") {
                throw "com.yung.util.TreeHashSet is not function!";
            }
            this.instance = new type();
            if (this.instance.classProp != null && this.instance.classProp.name != null) {
                this.instanceName = this.instance.classProp.name;
            } else {
                this.instanceName = this.instance.constructor.name;
            }
            if (typeof this.instance.compareTo != 'function') {
                throw "Please implement compareTo function in " + this.instanceName;
            }
            if (reverse === true) {
                this.reverse = true;
            } else {
                this.reverse = false;
            }
            this.type = type;
            this.hashArray = [];
            this.array = [];
            return this;
        },
        add : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                throw "argument type is not " + this.instanceName;
            }
            var hashCode = element.hashCode();
            if (jQuery.inArray(hashCode, this.hashArray) < 0) {
                this.hashArray.push(hashCode);
                this.array.push(element);
            }
            this.sort(this.reverse);
        }
    });
    
    var com_yung_util_BasicList = com_yung_util_Collection.extend({
        classProp : { name : "com.yung.util.BasicList"},
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.List type can not be null!";
            }
            if (typeof type == "string") {
                type = type.toLowerCase();
            }
            if (type != 'string' && type != 'number') {
                throw "com.yung.util.List type can only be string or number!";
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        add : function (arg1, arg2) {
            if (arg2 == null) {
                this.addElement(arg1);
            } else {
                this.addByIndex(arg1, arg2);
            }
        },
        addElement : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                throw "argument type must be " + this.type;
            }
            this.array.push(element);
        },
        addByIndex : function (index, element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                throw "argument type must be " + this.type;
            }
            this.array.splice(index, 0, element);
        },
        addAll : function (arg1, arg2) {
            if (arg2 == null) {
                this.addAllElements(arg2);
            } else {
                this.addAllByIndex(arg1, arg2);
            }
        },
        addAllElements : function (list) {
            if (list == null) {
                throw "argument can not be empty!";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (list.type != this.type) {
                throw "argument type must be " + this.type;
            }
            for (var i = 0; i < list.array.length; i++) {
                this.add(list.array[i]);
            }
            return true;
        },
        addAllByIndex : function (index, list) {
            if (list == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (list.type != this.type) {
                throw "argument type must be " + this.type;
            }
            var idx = index;
            for (var i = 0; i < list.array.length; i++) {
                this.addByIndex(idx, list.array[i]);
                idx++;
            }
            return true;
        },
        equals : function (list) {
            if (list == null) {
                return false;
            }
            if (this.isCollection(list) == false) {
                return false;
            }
            if (list.array.length != list.array.length) {
                return false;
            }
            for (var i = 0; i < this.array.length; i++) {
                if (this.array[i] != list.array[i]) {
                    return false;
                }
            }
            return false;
        },
        get : function (index) {
            return this.array[index];
        },
        indexOf : function (element) {
            if (element == null) {
                return -1;
            }
            if (typeof element != this.type) {
                return -1;
            }
            for (var i = 0; i < this.array.length; i++) {
                if (element === this.array[i]) {
                    return i;
                }
            }
            return -1;
        },
        lastIndexOf : function (element) {
            if (element == null) {
                return -1;
            }
            if (typeof element != this.type) {
                return -1;
            }
            for (var i = this.array.length - 1; i >= 0; i--) {
                if (element === this.array[i]) {
                    return i;
                }
            }
            return -1;
        },
        removeByIndex : function (index) {
            if (index < 0 || index >= this.array.length) {
                return null;
            }
            var ret = this.array[index];
            this.array.splice(index, 1);
            return ret;
        },
        removeElement : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != this.type) {
                return false;
            }
            var index = jQuery.inArray(element, this.array);
            if (index < 0) {
                return false;
            } else {
                if (index !== -1) {
                    this.array.splice(index, 1);
                    return true;
                } else {
                    return false;
                }
            }
        },
        removeAll : function (list) {
            if (list == null) {
                return false;
            }
            if (this.isCollection(list) == false) {
                return false;
            }
            for (var i = 0; i < list.array.length; i++) {
                this.removeElement(list.array[i]);
            }
            return true;
        },
        retainAll : function (list) {
            if (list == null) {
                return false;
            }
            if (this.isCollection(list) == false) {
                return false;
            }
            var tempArray = [];
            for (var i = 0; i < this.array.length; i++) {
                if (list.contains(this.array[i])) {
                    tempArray.push(this.array[i]);
                }
            }
            this.array = tempArray;
            return true;
        },
        set : function (index, element) {
            this.array[index] = element;
            return element;
        },
        subList : function (fromIndex, toIndex) {
            var ret = new com.yung.util.List(this.type);
            for (var i = 0; i < this.array.length; i++) {
                if (i >= fromIndex && i <= toIndex) {
                    ret.add(this.array[i]);
                }
            }
            return ret;
        }
    });
    
    var com_yung_util_ArrayList = com_yung_util_BasicList.extend({
        classProp : { name : "com.yung.util.ArrayList"},
        init : function(debug){
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = Array;
            this.array = [];
            return this;
        },
        add : function (arg1, arg2) {
            if (arg2 == null) {
                this.addElement(arg1);
            } else {
                this.addByIndex(arg1, arg2);
            }
        },
        addElement : function (element) {
            if (element == null) {
                return false;
            }
            if (!jQuery.isArray(element)) {
                throw "argument type must be Array";
            }
            this.array.push(element);
        },
        addByIndex : function (index, element) {
            if (element == null) {
                return false;
            }
            if (!jQuery.isArray(element)) {
                throw "argument type must be Array";
            }
            this.array.splice(index, 0, element);
        },
        addAll : function (arg1, arg2) {
            if (arg2 == null) {
                this.addAllElements(arg2);
            } else {
                this.addAllByIndex(arg1, arg2);
            }
        },
        addAllElements : function (list) {
            if (list == null) {
                throw "argument can not be empty!";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (list.type != this.type) {
                throw "argument type must be Array";
            }
            for (var i = 0; i < list.array.length; i++) {
                this.add(list.array[i]);
            }
            return true;
        },
        addAllByIndex : function (index, list) {
            if (list == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (list.type != this.type) {
                throw "argument type must be Array";
            }
            var idx = index;
            for (var i = 0; i < list.array.length; i++) {
                this.addByIndex(idx, list.array[i]);
                idx++;
            }
            return true;
        }
    });
    
    var com_yung_util_RawObjectList = com_yung_util_BasicList.extend({
        classProp : { name : "com.yung.util.RawObjectList"},
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.RawObjectList type can not be null!";
            }
            if (type != 'object') {
                throw "com.yung.util.RawObjectList type can only be object";
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        add : function (arg1, arg2) {
            if (arg2 == null) {
                this.addElement(arg1);
            } else {
                this.addByIndex(arg1, arg2);
            }
        },
        addElement : function (element) {
            if (element == null) {
                return false;
            }
            if (typeof element != 'object') {
                throw "argument type must be object";
            }
            this.array.push(element);
        },
        addByIndex : function (index, element) {
            if (element == null) {
                return false;
            }
            if (typeof element != 'object') {
                throw "argument type must be object";
            }
            this.array.splice(index, 0, element);
        },
        addAll : function (arg1, arg2) {
            if (arg2 == null) {
                this.addAllElements(arg2);
            } else {
                this.addAllByIndex(arg1, arg2);
            }
        },
        addAllElements : function (list) {
            if (list == null) {
                throw "argument can not be empty!";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (typeof element != 'object') {
                throw "argument type must be object";
            }
            for (var i = 0; i < list.array.length; i++) {
                this.add(list.array[i]);
            }
            return true;
        },
        addAllByIndex : function (index, list) {
            if (list == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (typeof element != 'object') {
                throw "argument type must be object";
            }
            var idx = index;
            for (var i = 0; i < list.array.length; i++) {
                this.addByIndex(idx, list.array[i]);
                idx++;
            }
            return true;
        }
    });
    
    var com_yung_util_ObjectList = com_yung_util_BasicList.extend({
        classProp : { name : "com.yung.util.ObjectList"},
        instance : null,
        instanceName : null,
        init : function(type, debug){
            if (type == null) {
                throw "com.yung.util.ObjectList type can not be null!";
            }
            if (typeof type != 'function') {
                throw "com.yung.util.ObjectList type can only be function!";
            }
            this.instance = new type();
            if (this.instance.classProp != null && this.instance.classProp.name != null) {
                this.instanceName = this.instance.classProp.name;
            } else {
                this.instanceName = this.instance.constructor.name;
            }
            if (debug === true) {
                this.debug = true;
            } else {
                this.debug = false;
            }
            this.type = type;
            this.array = [];
            return this;
        },
        addElement : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                throw "argument type is not " + this.instanceName;
            }
            this.array.push(element);
        },
        addByIndex : function (index, element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                throw "argument type is not " + this.instanceName;
            }
            this.array.splice(index, 0, element);
        },
        addAllElements : function (list) {
            if (list == null) {
                throw "argument can not be empty!";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (list.type != this.type) {
                throw "argument type is not " + this.instanceName;
            }
            for (var i = 0; i < list.array.length; i++) {
                this.add(list.array[i]);
            }
            return true;
        },
        addAllByIndex : function (index, list) {
            if (list == null) {
                throw "argument can not be empty";
            }
            if (this.isCollection(list) == false) {
                throw "argument is not com.yung.util.Collection";
            }
            if (list.type != this.type) {
                throw "argument type is not " + this.instanceName;
            }
            var idx = index;
            for (var i = 0; i < list.array.length; i++) {
                this.addByIndex(idx, list.array[i]);
                idx++;
            }
            return true;
        },
        contains : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                return false;
            }
            if (jQuery.inArray(element, this.array) < 0) {
                return false;
            } else {
                return true;
            }
        },
        indexOf : function (element) {
            if (element == null) {
                return -1;
            }
            if (this.validType(element) == false) {
                return -1;
            }
            for (var i = 0; i < this.array.length; i++) {
                if (element === this.array[i]) {
                    return i;
                }
            }
            return -1;
        },
        lastIndexOf : function (element) {
            if (element == null) {
                return -1;
            }
            if (this.validType(element) == false) {
                return -1;
            }
            for (var i = this.array.length - 1; i >= 0; i--) {
                if (element === this.array[i]) {
                    return i;
                }
            }
            return -1;
        },
        removeElement : function (element) {
            if (element == null) {
                return false;
            }
            if (this.validType(element) == false) {
                return false;
            }
            var index = jQuery.inArray(element, this.array);
            if (index < 0) {
                return false;
            } else {
                if (index !== -1) {
                    this.array.splice(index, 1);
                    return true;
                } else {
                    return false;
                }
            }
        },
        subList : function (fromIndex, toIndex) {
            var ret = new com.yung.util.ObjectList(this.type);
            for (var i = 0; i < this.array.length; i++) {
                if (i >= fromIndex && i <= toIndex) {
                    ret.add(this.array[i]);
                }
            }
            return ret;
        }
    });
    
    var com_yung_util_Set = function (type, debug) {
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type == 'string' || type == 'number') {
            return new com.yung.util.BasicSet(type);
        }
        if (type == Array) {
            return new com.yung.util.ArraySet();
        }
        if (type == 'object') {
            return new com.yung.util.RawObjectSet(type);
        }
        if (typeof type == 'function') {
            return new com.yung.util.ObjectSet(type, debug);
        } else {
            throw "argument type not match any type";
        }
    }
    $Y.register("com.yung.util.Set", com_yung_util_Set);
    
    var com_yung_util_List = function (type, debug) {
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type == 'string' || type == 'number') {
            return new com.yung.util.BasicList(type);
        }
        if (type == Array) {
            return new com.yung.util.ArrayList();
        }
        if (type == 'object') {
            return new com.yung.util.RawObjectList(type);
        }
        if (typeof type == 'function') {
            return new com.yung.util.ObjectList(type, debug);
        } else {
            throw "argument type not match any type";
        }
    }
    $Y.register("com.yung.util.List", com_yung_util_List);
    
    