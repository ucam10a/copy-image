    var com_yung_util_Popup = $Class.extend({
        classProp : { name : "com.yung.util.Popup" },
        currentPopupIndex : null,
        canCloseWindow : null,
        init : function () {
            if (yung_global_var["com_yung_util_Popup"] != null) {
                throw "Please use com.yung.util.Popup.instance() to create instance!";
            }
            this.currentPopupIndex = 0;
            this.canCloseWindow = true;
            com_yung_util_Popup.instance(this);
            return this;
        },
        createPopoupFrame : function (index) {
            var popup = jQuery("#popup-background-" + index)[0];
            if (popup == null) {
                var obj = {};
                obj["index"] = index;
                var template = new com.yung.util.BasicTemplate(obj);
                template.add('<div id="popup-background-{{index}}" style="display: none; position: absolute; left: 0px; top: 0px; width: 100%; height: 2000px; z-index: 10{{index}}00; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);" ></div>');
                template.add('    <div id="popup-holder-{{index}}" class="dialog-border" style="display: none; position: absolute; left: 0px; top: 0px; border-radius: 3px; background-color: rgb(204, 216, 231); z-index: 10{{index}}01; width: 200px; height: 100px; box-shadow: 5px 5px 5px #d7d7d7;" >');
                template.add('    <div id="popup-titlebar-{{index}}" class="dialog-titlebar" style="cursor: move;" >');
                template.add('        <div id="popup-title-{{index}}" style="float: left; padding-left: 18px; padding-top: 3px; "></div><div style="height: 18px;" class="dialog-closeButton" onclick="com_yung_util_Popup.instance().closePopup()"><span style="color: red;" title="close">&#10006;</span></div>');
                template.add('    </div>');
                template.add('    <div id="iframe-holder-{{index}}" style="margin: 5px; display: none; background: white;" height="100%">');
                template.add('        <iframe id="popup-content-{{index}}" src="" width="99%" frameborder="0" style="height: 100%; border-style: none transparent;" onload="com_yung_util_Popup.instance().showIframe({{index}});"></iframe>');
                template.add('    </div>');
                template.add('</div>');
                var html = template.toHtml();
                jQuery("body").append(html);
                new com.yung.util.Movable("popup-titlebar-" + index, "popup-holder-" + index);
		    }
        },
        setCanCloseWindow : function (canClose) {
            this.canCloseWindow = canClose;
        },
        showIframe : function (index, show) {
            if (show == false) {
                jQuery("#iframe-holder-" + index).css("display", "none");
                return;    
            }
            jQuery("#iframe-holder-" + index).css("display", "block");
        },
        closePopup : function (index, callback) {
            if (this.canCloseWindow) {
                if (index == null) {
                    index = this.currentPopupIndex;
                }
                var win = this.getIframeWin();
                if (typeof win["closePopupBefore"] == "function") {
                    win["closePopupBefore"]();
                }
                this.showIframe(index, false);
                document.getElementById('popup-background-' + index).style.display = 'none';
                document.getElementById('popup-holder-' + index).style.display = 'none';
                this.currentPopupIndex = this.currentPopupIndex - 1;
                if (typeof callback === "function") {
                    callback();
                }
            } else {
                alert("Can not close window now!");
            }
        },
        openPopup : function (title, width, height, url, pos, index) {
            var windowHeight = jQuery(document).height();
            var windowWidth = jQuery(document).width();
            var top = ((windowHeight - height) / 2) * 1.0;
            if (pos == null) {
                pos = {};
                pos.top = top;
            }
            if (top < pos.top) {
                if (pos.top < (windowHeight / 2)) {
                    top = pos.top + 30;
                }
                pos.top = top;
            }
            this.openPopupWithPos(title, width, height, url, pos, index);
        },
        openPopupWithPos : function (title, width, height, url, pos, index) {
            if (index == null) {
                this.currentPopupIndex = this.currentPopupIndex + 1;
                index = this.currentPopupIndex;
            }
            this.createPopoupFrame(index);
            var windowHeight = jQuery(document).height();
            var windowWidth = jQuery(document).width();
            var top = pos.top;
            var left = ((windowWidth - width) / 2) * 1.0;
            document.getElementById('popup-holder-' + index).style.top = top;
            document.getElementById('popup-holder-' + index).style.left = left;
            document.getElementById('popup-title-' + index).innerHTML = title;
            document.getElementById('popup-holder-' + index).style.height = height;
            document.getElementById('popup-holder-' + index).style.width = width;
            document.getElementById('popup-content-' + index).style.height = height - 50;
            document.getElementById('popup-content-' + index).src = url;
            document.getElementById('popup-background-' + index).style.display = 'block';
            document.getElementById('popup-background-' + index).style.height = windowHeight + 'px';
            document.getElementById('popup-background-' + index).style.width = windowWidth + 'px';
            document.getElementById('popup-holder-' + index).style.display = 'block';
        },
        getIframeWin : function (index) {
            if (index == null) {
                index = this.currentPopupIndex;
            }
            var ele = document.getElementById("popup-content-" + index);
            if (ele == null) {
                return null;
            }
            var win = document.getElementById("popup-content-" + index).contentWindow;
            return win;
        },
        getParentWin : function () {
            var win = this.getIframeWin(this.currentPopupIndex - 1);
            if (win == null) {
                return window;
            } else {
                return win;
            }
        }
    });
    com_yung_util_Popup.instance = function(popup) {
        if (popup != null) {
            if (popup instanceof com_yung_util_Popup) {
                yung_global_var["com_yung_util_Popup"] = popup;
            }
        } else {
            try {
                if (window.parent.yung_global_var != null && window.parent.yung_global_var["com_yung_util_Popup"] != null) {
                    return window.parent.yung_global_var["com_yung_util_Popup"];
                }    
            } catch (e) {
                // prevent permission denied
            }
            var instance = yung_global_var["com_yung_util_Popup"];
            if (instance == null) {
                instance = new com_yung_util_Popup();
            }
            return instance;
        }
    }
    jQuery( document ).ready(function() {
        if (typeof com_yung_util_Movable == 'undefined') {
            alert("yung-Popup.js requires yung-Movable.js!");
        }
        if (typeof com_yung_util_BasicTemplate == 'undefined') {
            alert("yung-Popup.js requires yung-BasicTemplate.js!");
        }
    });