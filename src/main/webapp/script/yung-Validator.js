var com_yung_util_Validator = {
    classProp : { name : "com.yung.util.Validator"},
    email : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input)) {  
            return true;
        }  
        return false;
    },
    digit : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^\d+$/.test(input)) {
            return true;
        }
        return false;
    },
    account : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[A-Za-z0-9_.]+$/.test(input)) {
            return true;
        }
        return false;
    },
    letter : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[A-Za-z_.]+$/.test(input)) {
            return true;
        }
        return false;
    },
    letterExtchar : function (input, specialChars) {
        if (input == null || input == '') {
            return true;
        }
        if (this.ascii(input) == false) {
            return false;
        }
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (this.letter(c) == false) {
                var valid = false;
                for (var j = 0; j < specialChars.length; j++) {
                    if (c == specialChars[j]) {
                        valid = true;
                        break;
                    }
                }
                if (valid == false) {
                    return false;
                }
            }
        }
        return true;
    },
    ascii : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[\x00-\x7F]*$/.test(input)) {
            return true;
        }
        return false;
    },
    accountExtchar : function (input, specialChars) {
        if (input == null || input == '') {
            return true;
        }
        if (this.ascii(input) == false) {
            return false;
        }
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (this.account(c) == false) {
                var valid = false;
                for (var j = 0; j < specialChars.length; j++) {
                    if (c == specialChars[j]) {
                        valid = true;
                        break;
                    }
                }
                if (valid == false) {
                    return false;
                }
            }
        }
        return true;
    }
};
$Y.register("com.yung.util.Validator", com_yung_util_Validator);