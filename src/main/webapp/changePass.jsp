<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>基金</title>
<%@ include file="inlineJSP/headElements.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<div id="main">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content">
        <br><br>
        <table align="center">
            <tr>
                <td>
                    <s:form id="changePassForm" name="changePassForm" action="changePass" theme="simple" method="POST" >
                        <s:property value="chMsgMap['old.pass']" /><br>
                        <input name="oldPassword" type="password" value="" /><br><br>
                        <s:property value="chMsgMap['new.pass']" /><br>
                        <input name="newPassword" type="password" value="" /><br><br>
                        <s:property value="chMsgMap['confirm.pass']" /><br>
                        <input name="confirmPassword" type="password" value="" /><br><br>
                        <input type="button" id="changePassSubmit" value='<s:property value="chMsgMap['submit']" />' onclick="changePass();" />
                    </s:form>
                </td>
            </tr>
        </table>
        
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
</body>
</html>