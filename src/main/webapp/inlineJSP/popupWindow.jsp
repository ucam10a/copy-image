<!-- The popup window for edit comment Dialog result -->
<div id="editCommentDialog" title="<s:property value="chMsgMap['comment']" /><s:property value="chMsgMap['edit']" />" style="overflow-x:auto; overflow-y:auto;">
    <div>
        <textarea id="comment-editor-comment" cols="85" rows="17" ></textarea>
        <input id="comment-editor-fundId" type="hidden" value="" />
		<br><br>
        <input type="button" value="<s:property value="chMsgMap['submit']" />" onclick="submitFundComment(<s:property value="soldHistory" />)" />
    </div>
</div>
<script type="text/javascript" language="JavaScript">
$("#editCommentDialog").dialog({
    autoOpen: false,
    height: 500,
    width: 850,
    modal: true,
    buttons: {
    	\u53D6\u6D88 : function() {
            $( this ).dialog( "close" );
        }
    },
    close: function() {
    }
});
</script>
<!-- The popup window for delete fund Dialog result -->
<div id="sellFundDialog" title="<s:property value="chMsgMap['sell.fund']" />" style="overflow-x:auto; overflow-y:auto;">
    <table>
        <tr>
        <td>
            <h2><s:property value="chMsgMap['buy.fund.info']" /> - <span id="sellFund-name"></span></h2>
            <table border=1 >
                <tr>
                    <td><s:property value="chMsgMap['fund.price']" /></td>
                    <td><span id="sellFund-price" ></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['fund.unit']" /></td>
                    <td><span id="sellFund-unit"></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['buy.date']" /></td>
                    <td><span id="sellFund-buyDate"></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['buy.exchange']" /></td>
                    <td><span id="sellFund-exchange" ></span></td>
                </tr>
            </table>
        </td>
        <td>
            <br>
            <h2> &nbsp; </h2>
            <table>
                <tr>
                    <td><s:property value="chMsgMap['sell.unit']" /> <span id="max-sell-fund-unit"></span></td>
                    <td><input id='sell-fund-unit' type="text" size=10 value='' /></td>
                </tr>
                <tr><td></td><td align="right"><input type="button" value='<s:property value="chMsgMap['submit']" />' onclick="sellFund();" /></td></tr>
            </table>
            <input id='sell-fund-id' name="fundId" type="hidden" value='' />
        </td>
        </tr>
    </table>
    
</div>
<script type="text/javascript" language="JavaScript">
$("#sellFundDialog").dialog({
    autoOpen: false,
    height: 500,
    width: 850,
    modal: true,
    buttons: {
        \u53D6\u6D88 : function() {
            $( this ).dialog( "close" );
        }
    },
    close: function() {
    }
});
</script>
<!-- The popup window for change fund Dialog -->
<div id="changeFundDialog" title="<s:property value="chMsgMap['change.fund']" />" style="overflow-x:auto; overflow-y:auto;">
    <table>
        <tr>
        <td>
            <h2><s:property value="chMsgMap['buy.fund.info']" /> - <span id="changeFund-name"></span></h2>
            <table border=1 >
                <tr>
                    <td><s:property value="chMsgMap['fund.price']" /></td>
                    <td><span id="changeFund-price" ></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['fund.unit']" /></td>
                    <td><span id="changeFund-unit"></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['buy.date']" /></td>
                    <td><span id="changeFund-buyDate"></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['buy.currency']" /></td>
                    <td><span id="changeFund-currency" ></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['current.value']" /></td>
                    <td><span id="changeFund-currentValue" ></span></td>
                </tr>
            </table>
        </td>
        <td>
            <br>
            <h2> &nbsp; </h2>
            <table>
                <tr>
                    <td><span id="change-origin-name"></span><s:property value="chMsgMap['change.value']" />(<span id="change-origin-currency"></span>)</td>
                    <td><input id='change-fund-value' type="text" size=10 value='' /></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['change.price']" /></td>
                    <td><input id='change-fund-price' type="text" size=10 value='' /></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['change.name']" /></td>
                    <td>
                        <input id='change-fund-name' type="text" size=10 value='' /> &nbsp;
                        <select onchange="selectFundName(this, 'change-fund-name');" >
	                        <option value=""></option>
	                        <s:iterator value="fundType" id="fund">
	                            <option value='<s:property value="fundName" />'><s:property value="fundName" /></option>
	                        </s:iterator>
	                    </select>
                    </td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['change.currency']" /></td>
                    <td>
                    	<select id="change-fund-currency" name="currency" onchange="changeCurrency(this, 'change-fund-exchange');" >
	                        <s:iterator value="currencyType" id="currency">
	                            <option value='<s:property value="currency" />'><s:property value="currencyName" /></option>
	                        </s:iterator>
	                    </select>
                    </td>
                </tr>
                <tr><td></td><td align="right"><input type="button" value='<s:property value="chMsgMap['submit']" />' onclick="changeFund();" /></td></tr>
            </table>
            <input id='change-fund-id' type="hidden" value='' />
        </td>
        </tr>
    </table>
    
</div>
<script type="text/javascript" language="JavaScript">
$("#changeFundDialog").dialog({
    autoOpen: false,
    height: 500,
    width: 850,
    modal: true,
    buttons: {
        \u53D6\u6D88 : function() {
            $( this ).dialog( "close" );
        }
    },
    close: function() {
    }
});
</script>