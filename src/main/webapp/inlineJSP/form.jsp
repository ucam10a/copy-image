<!-- The hidden ajax forms -->
<div style="display:none;" >
    <s:form id="showAllFundByDayForm" name="showAllFundByDayForm" action="showAllFund" theme="simple" method="POST" >
        <input id="showAllFundByDayForm-day" name="day" type="hidden" value="" />
        <s:submit id="showAllFundSubmit" />
    </s:form>
	<s:form id="updateExchangeHistoryForm" name="updateExchangeHistoryForm" action="updateExchangeHistory" theme="simple" method="POST" >
		<input id="updateExchangeHistory-currency" name="currency" type="hidden" value="" />
		<input name="date" type="hidden" value="<s:property value="day" />" />
		<input id="updateExchangeHistory-rate" name="rate" type="hidden" value="" />
		<s:submit id="updateExchangeHistorySubmit" />
	</s:form>
	<s:form id="updateFundPriceForm" name="updateFundPriceForm" action="updateFundHistory" theme="simple" method="POST" >
        <input id="updateFundPrice-md5FundName" name="md5FundName" type="hidden" value="" />
        <input name="date" type="hidden" value="<s:property value="day" />" />
        <input id="updateFundPrice-fundPrice" name="price" type="hidden" value="" />
        <s:submit id="updateFundHistorySubmit" />
    </s:form>
    <s:form id="updateFundUnitForm" name="updateFundUnitForm" action="updateFundUnit" theme="simple" method="POST" >
        <input id="updateFundUnit-id" name="id" type="hidden" value="" />
        <input name="date" type="hidden" value="<s:property value="day" />" />
        <input id="updateFundUnit-fundUnit" name="unit" type="hidden" value="" />
        <s:submit id="updateFundUnitSubmit" />
    </s:form>
    <s:form id="numberValidateForm" name="numberValidateForm" action="numberValidate" theme="simple" method="POST" >
        <input id="numberValidateForm-num1" name="num1" type="hidden" value="" />
        <input id="numberValidateForm-num2" name="num2" type="hidden" value="" />
        <input id="numberValidateForm-num3" name="num3" type="hidden" value="" />
        <input id="numberValidateForm-num4" name="num4" type="hidden" value="" />
        <s:submit id="numberValidateSubmit" />
    </s:form>
    <s:form id="dateValidateForm" name="dateValidateForm" action="dateValidate" theme="simple" method="POST" >
        <input id="dateValidateForm-date1" name="date1" type="hidden" value="" />
        <input id="dateValidateForm-date2" name="date2" type="hidden" value="" />
        <input id="dateValidateForm-date3" name="date3" type="hidden" value="" />
        <input id="dateValidateForm-date4" name="date4" type="hidden" value="" />
        <s:submit id="dateValidateSubmit" />
    </s:form>
    <s:form id="updateFundCommentForm" name="updateFundCommentForm" action="updateFundComment" theme="simple" method="POST" >
        <input id="updateFundComment-id" name="id" type="hidden" value="" />
        <input id="updateFundComment-comment" name="comment" type="hidden" value="" />
        <s:submit id="updateFundCommentSubmit" />
    </s:form>
    <s:form id="delFundForm" name="delFundForm" action="delFund" theme="simple" method="POST" >
        <input id="delFund-id" name="id" type="hidden" value="" />
        <input id="delFund-moveDate" name=moveDate type="hidden" value="" />
        <s:submit id="delFundSubmit" />
    </s:form>
    <s:form id="sellFundForm" name="sellFundForm" action="sellFund" theme="simple" method="POST" >
        <input id="sellFundForm-id" name="id" type="hidden" value="" />
        <input id="sellFundForm-sellDate" name="sellDate" type="hidden" value="" />
        <input id="sellFundForm-unit" name="unit" type="hidden" value="" />
        <s:submit id="sellFundSubmit" />
    </s:form>
    <s:form id="changeFundDateForm" name="changeFundDateForm" action="changeFundDate" theme="simple" method="POST" >
        <input id="changeFundDateForm-date" name="date" type="hidden" value="" />
        <s:submit id="changeFundDateSubmit" />
    </s:form>
    <s:form id="changeFundForm" name="changeFundForm" action="changeFund" theme="simple" method="POST" >
        <input id="changeFundForm-id" name="id" type="hidden" value="" />
        <input id="changeFundForm-price" name="price" type="hidden" value="" />
        <input id="changeFundForm-date" name="changeDate" type="hidden" value="" />
        <input id="changeFundForm-value" name="value" type="hidden" value="" />
        <input id="changeFundForm-name" name="buyFundName" type="hidden" value="" />
        <input id="changeFundForm-md5" name="buyFundMd5" type="hidden" value="" />
        <input id="changeFundForm-currency" name="currency" type="hidden" value="" />
        <s:submit id="changeFundSubmit" />
    </s:form>
</div>