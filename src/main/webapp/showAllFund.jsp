<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><s:property value="chMsgMap['main.menu']" /></title>
<%@ include file="inlineJSP/headElements.jsp" %>
<%@ include file="inlineJSP/ajaxDialog.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<div id="main">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content">
        <br>
        <table align="center">
            <tr>
                <td><b>
                    <s:property value="chMsgMap['now.date']" /> 
                    <select id="nowDay" onchange="changeDate(this);" >
                        <option value='<s:property value="day" />'><s:property value="day" /></option>
                        <s:iterator value="historyDates" id="histroyDate">
                            <option value="<s:property value="date" />"><s:property value="date" /></option>
                        </s:iterator>
                    </select>
                </b></td>
                <td>
                    <s:property value="chMsgMap['select.date']" /> <br>
                    <s:property value="chMsgMap['date']" /> : <input id="showAllFund-date" type="text" value='' placeholder="<s:property value="chMsgMap['year.month.day']" />" onKeyPress="return submitenter(event, showAllFund)" />
                </td>
            </tr>
        </table>
        <table align="center">
            <tr>
                <td>
                    <table border="1" >
                        <tr>
                            <td width="75" ><s:property value="chMsgMap['currency']" /></td>
			                <td width="250" ><s:property value="chMsgMap['exchange']" /></td>
			            </tr>
			            <s:iterator value="exchanges" id="exchange">
			            <tr>
			                <td height="30" ><s:property value="currencyName" /></td>
			                <td><div id="<s:property value="currency" />">
			                    <s:if test="exchangeValue == '0.0'">
			                        <input id='<s:property value="currency" />-exchangeRate' name="rate" type="text" size=10 value='' placeholder='<s:property value="chMsgMap['please.insert']" /><s:property value="chMsgMap['exchange']" />' onKeyPress="return submitenter(event, editExchange, '<s:property value="currency" />')" /> <s:property value="chMsgMap['nt.dollar']" /><br>
			                        <font color="red"><b><s:property value="chMsgMap['not.edit']" />, <s:property value="chMsgMap['please.insert']" /></b></font>
			                    </s:if>
			                    <s:else>
			                        <input id='<s:property value="currency" />-exchangeRate' name="rate" type="text" size=10 value='<s:property value="exchangeValue" />' onKeyPress="return submitenter(event, editExchange, '<s:property value="currency" />')" /> <s:property value="chMsgMap['nt.dollar']" /><br>
			                    </s:else>
			                </div></td>
			            </tr>
			            </s:iterator>
                    </table>
                </td>
            </tr>
        </table>
        <table align="center">
            <tr>
                <td><font size="+2"><b>
                    <input type="button" value="<s:property value="chMsgMap['del.fund']" />" onclick="deleteFundDate();" /> 
                    <span id="displayFundDate" style="cursor:pointer" title="<s:property value="chMsgMap['modify.date']" />" onclick="modifyFundDate('<s:property value="day" />');" ><s:property value="day" /></span> 
                    <span id="changeFundDate"></span> 
                    <s:property value="chMsgMap['current.fund.detail']" />
                    <s:property value="chMsgMap['fund.toal.origin.value']" /> <s:property value="totalNTValue" /> <s:property value="chMsgMap['nt.dollar']" /> 
                    <span style="display: none;" ><s:property value="totalBalance" /></span>
                    <s:if test="totalBalance < -90000000 ">
                    </s:if>
                    <s:else>
                        <s:if test="totalBalance >= 0">
                            <font color="red"><b>+<s:property value="totalBalance" /><s:property value="chMsgMap['nt.dollar']" /></b></font>
                        </s:if>
                        <s:else>
                            <font color="green"><b><s:property value="totalBalance" /><s:property value="chMsgMap['nt.dollar']" /></b></font>
                        </s:else>
                    </s:else>
                </b></font></td>
            </tr>
        </table>
        <table align="center" border="1" >
            <tr>
                <td width="130" ><s:property value="chMsgMap['fund.name']" /></td>
                <td width="70" ><s:property value="chMsgMap['buy.date']" /></td>
                <td width="70" ><s:property value="chMsgMap['fund.price']" /></td>
                <td width="120" ><s:property value="chMsgMap['today.price']" /></td>
                <td width="120" ><s:property value="chMsgMap['fund.unit']" /></td>
                <td width="45" ><s:property value="chMsgMap['currency']" /></td>
                <td width="60" ><s:property value="chMsgMap['buy.value']" /></td>
                <td width="150" ><s:property value="chMsgMap['current.value']" /></td>
                <td width="250" ><s:property value="chMsgMap['comment']" /></td>
                <td width="70" ><s:property value="chMsgMap['sell.fund']" /></td>
            </tr>
            <s:iterator value="funds" id="fund">
            <tr>
                <td height="60" align="center">
                    <a href="javascript:void(null);" onclick="" ><s:property value="fundName" /></a>
                    <br>
                    <!-- <input type="button" onclick="inputChangeFund('<s:property value="id" />', '<s:property value="fundName" />', '<s:property value="unit" />', '<s:property value="buyDateStr" />', '<s:property value="buyPrice" />', '<s:property value="buyExchangeRate" />');" value='<s:property value="chMsgMap['change.fund']" />' /> -->
                </td>
                <td><s:property value="buyDateStr" /></td>
                <td><s:property value="buyPriceStr" /></td>
                <td align="center"><div id='<s:property value="id" />-<s:property value="md5FundName" />'>
                    <s:if test="currentFundPrice == '0.0' ">
                        <input id='<s:property value="md5FundName" />-fundPrice' type="text" size=15 value='' placeholder='<s:property value="chMsgMap['please.insert']" /><s:property value="chMsgMap['today.price']" />' onKeyPress="return submitenter(event, editFundPrice, '<s:property value="id" />', '<s:property value="md5FundName" />')" /><br>
                        <font color="red"><b><s:property value="chMsgMap['not.edit']" />, <s:property value="chMsgMap['please.insert']" /></b></font>
                    </s:if>
                    <s:else>
                        <input id='<s:property value="md5FundName" />-fundPrice' type="text" size=15 value='<s:property value="currentFundPrice" />' onKeyPress="return submitenter(event, editFundPrice, '<s:property value="id" />', '<s:property value="md5FundName" />')" /><br>
                    </s:else>
                </div></td>
                <td>
                    <s:property value="chMsgMap['origin.unit']" /> : <s:property value="originBuyUnit" />
                    <s:if test="currentFundUnit == '0.0' ">
                        <input id='<s:property value="id" />-fundUnit' type="text" size=17 value='' placeholder="<s:property value="chMsgMap['please.insert']" /><s:property value="chMsgMap['today.unit']" />" onKeyPress="return submitenter(event, editFundUnit, '<s:property value="id" />', '<s:property value="md5FundName" />')" /><br>
                        <font color="red"><b><s:property value="chMsgMap['not.edit']" />, <s:property value="chMsgMap['please.insert']" /></b></font>
                    </s:if>
                    <s:else>
                        <input id='<s:property value="id" />-fundUnit' type="text" size=17 value='<s:property value="currentFundUnit" />' onKeyPress="return submitenter(event, editFundUnit, '<s:property value="id" />', '<s:property value="md5FundName" />')" /><br>
                    </s:else>
                </td>
                <td><span id='<s:property value="id" />-fundCurrency'><s:property value="currencyName" /></span></td>
                <td><s:property value="buyMoney" /></td>
                <td><div id="<s:property value="id" />-currentValue">
                    <s:if test="currentValue <= 0"> 
                        <font color="red"><b><s:property value="chMsgMap['not.edit']" /></b></font>
                    </s:if>
                    <s:else>
                        <s:if test="currentNTValue > 0">
                            <s:property value="currentNTValue" /> <s:property value="chMsgMap['nt.dollar']" /><br>
                            <s:if test="NTBalance >= 0">
                                <font color="red"><b>+<s:property value="NTBalance" /> [ +<s:property value="NTBalancePercent" />% ] </b></font><br>
                            </s:if>
                            <s:else>
                                <font color="green"><b><s:property value="NTBalance" /> [ <s:property value="NTBalancePercent" />% ] </b></font><br>
                            </s:else>
                        </s:if>
                        <input id="<s:property value="id" />-currentValue-hidden" type="hidden" value="<s:property value="currentValue" />" />
                        <s:property value="currentValue" /> <s:property value="currencyName" /><br>
                        <s:if test="balance >= 0">
                            <font color="red"><b>+<s:property value="balance" /> [ +<s:property value="balancePercent" />% ] </b></font>
                        </s:if>
                        <s:else>
                            <font color="green"><b><s:property value="balance" /> [ <s:property value="balancePercent" />% ] </b></font>
                        </s:else>
                    </s:else>
                </div></td>
                <td>
                    <s:property value="chMsgMap['buy.exchange']" /> : <s:property value="buyExchangeRate" /><br>
                    <span><s:property escape="false" value="commentHtml" /></span> &nbsp; <input type="button" value="<s:property value="chMsgMap['edit']" />" onclick="editFundComment('<s:property value="id" />')" />
                    <div id="<s:property value="id" />-comment-text" style="display: none;" ><s:property value="comment" /></div>
                </td>
                <td align="center">
                    <!-- <div style="height: 25px;"><input type="button" value="<s:property value="chMsgMap['sell.fund']" />" onclick="inputSellFund('<s:property value="id" />', '<s:property value="fundName" />', '<s:property value="unit" />', '<s:property value="buyDateStr" />', '<s:property value="buyPrice" />', '<s:property value="buyExchangeRate" />');" /></div> -->
                    <div style="height: 25px;"><input type="button" value="<s:property value="chMsgMap['del.fund']" />" onclick="delFund('<s:property value="id" />', '<s:property value="fundName" />');" /></div>
                    <br>
                    <div style="height: 25px;"><input type="button" value="<s:property value="chMsgMap['move.fund']" />" onclick="moveFund('<s:property value="id" />', '<s:property value="fundName" />');" /></div>
                </td>
            </tr>    
            </s:iterator>
        </table>
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
<jsp:directive.include file="inlineJSP/popupWindow.jsp"/>
</body>
</html>