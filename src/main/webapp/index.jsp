<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>基金</title>
<%@ include file="inlineJSP/headElements.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<div id="main">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content">
        <table align="center">
            <tr><td>
            <s:if test="#session.login != 'true'">
                <h2> <span id="title" ></span></h2>
                <s:form action="login" theme="simple" method="POST">
                    <input type="password" name="password" /><br/><br/>
                    <input id="submit" type="submit" value="" />
               </s:form>
            </s:if>
            </td></tr>
        </table>
    </div>
    <script>
        document.getElementById("title").innerHTML = "\u8ACB\u8F38\u5165\u5BC6\u78BC";
        document.getElementById("submit").value = "\u9001\u51FA";
    </script>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
</body>
</html>