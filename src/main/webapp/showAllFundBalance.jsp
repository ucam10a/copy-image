<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>???</title>
<%@ include file="inlineJSP/headElements.jsp" %>
<%@ include file="inlineJSP/ajaxDialog.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<div id="main">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content">
        <br>
        <table align="center">
            <tr>
                <td>
                    <font size="+2"><b><s:property value="chMsgMap['fund.balance.comment']" /></b></font>
                </td>
            </tr>
        </table>
        <br>
        <table align="center" border="1" >
            <tr>
                <td width="130" ><s:property value="chMsgMap['fund.name']" /></td>
                <td width="100" ><s:property value="chMsgMap['fund.balance']" /></td>
            </tr>
            <s:iterator value="funds" id="fund">
            <tr>
                <td height="60" align="center">
                    <s:property value="fundName" />
                </td>
                <td>
                    <s:if test="totalBalanceVal >= 0">
                        <font color="red"><b>+<s:property value="totalBalance" /> <s:property value="currencyName" /> <br> +<s:property value="totalBalancePercent" />% </b></font>
                    </s:if>
                    <s:else>
                        <font color="green"><b><s:property value="totalBalance" /> <s:property value="currencyName" /> <br> <s:property value="totalBalancePercent" />%</b></font>
                    </s:else>
                </td>
            </tr>    
            </s:iterator>
        </table>
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
<jsp:directive.include file="inlineJSP/popupWindow.jsp"/>
</body>
</html>