<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><s:property value="chMsgMap['fund.history']" /></title>
<%@ include file="inlineJSP/headElements.jsp" %>
<%@ include file="inlineJSP/ajaxDialog.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<div id="main">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content">
        <br><br>
        <table align="center" border="1" >
            <tr>
                <td colspan="10" align="center" ><br>
                    <font size="+2" ><b><s:property value="chMsgMap['total.balance']" /> </b></font>
                    <s:if test="totalBalance >= 0">
                        <font color="red" size="+2" ><b>+<s:property value="totalBalance" /> <s:property value="chMsgMap['nt.dollar']" /> </b></font>
                    </s:if>
                    <s:else>
                        <font color="green" size="+2" ><b><s:property value="totalBalance" /> <s:property value="chMsgMap['nt.dollar']" /> </b></font>
                    </s:else>
                <br><br></td>
            </tr>
            <tr>
                <td width="130" ><s:property value="chMsgMap['fund.name']" /></td>
                <td width="70" ><s:property value="chMsgMap['buy.date']" /></td>
                <td width="70" ><s:property value="chMsgMap['fund.price']" /></td>
                <td width="70" ><s:property value="chMsgMap['buy.exchange']" /></td>
                <td width="45" ><s:property value="chMsgMap['currency']" /></td>
                <td width="100" ><s:property value="chMsgMap['sell.price']" /></td>
                <td width="100" ><s:property value="chMsgMap['sell.exchange']" /></td>
                <td width="100" ><s:property value="chMsgMap['sell.fund.unit']" /></td>
                <td width="200" ><s:property value="chMsgMap['balance']" /></td>
                <td width="100" ><s:property value="chMsgMap['comment']" /></td>
            </tr>
            <s:iterator value="soldFunds" id="sellFund">
            <tr>
                <td align="center"><s:property value="fundName" /></td>
                <td><s:property value="buyDateStr" /></td>
                <td><s:property value="buyPrice" /></td>
                <td><s:property value="buyExchangeRate" /></td>
                <td><s:property value="currencyName" /></td>
                <td><s:property value="soldPrice" /></td>
                <td><s:property value="soldExchangeRate" /></td>
                <td><s:property value="unit" /></td>
                <td>
                    <s:if test="fund.currency != 'taiwan'">
                        <s:if test="NTBalance >= 0">
                            <font color="red"><b>+<s:property value="NTBalance" /> <s:property value="chMsgMap['nt.dollar']" /> [ +<s:property value="NTBalancePercent" />% ] </b></font><br>
                        </s:if>
                        <s:else>
                            <font color="green"><b><s:property value="NTBalance" /> <s:property value="chMsgMap['nt.dollar']" /> [ <s:property value="NTBalancePercent" />% ] </b></font><br>
                        </s:else>
                    </s:if>
                    <s:if test="balance >= 0">
                        <font color="red"><b>+<s:property value="balance" /> <s:property value="fund.currencyName" /> [ +<s:property value="balancePercent" />% ] </b></font>
                    </s:if>
                    <s:else>
                        <font color="green"><b><s:property value="balance" /> <s:property value="fund.currencyName" /> [ <s:property value="balancePercent" />% ] </b></font>
                    </s:else>
                </td>
                <td>
                    <span><s:property escape="false" value="commentHtml" /></span> &nbsp; <input type="button" value="<s:property value="chMsgMap['edit']" />" onclick="editFundComment('<s:property value="fundId" />')" />
                    <div id="<s:property value="fundId" />-comment-text" style="display: none;" ><s:property value="comment" /></div>
                </td>
            </tr>    
            </s:iterator>
        </table>
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
<jsp:directive.include file="inlineJSP/popupWindow.jsp"/>
</body>
</html>