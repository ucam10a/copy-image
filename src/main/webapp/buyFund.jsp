<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>基金</title>
<%@ include file="inlineJSP/headElements.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body onload="changeCurrency();">
<div id="main">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content">
        <br><br><br>
        <s:form id="buyFundFrom" name="buyFundFrom" action="buyFund" theme="simple" method="POST" >
        <table align="center" border=1 >
            <tr height=30 >
                <td colspan=2 ><s:property value="chMsgMap['add.new.fund']" /></td>
            </tr>
            <tr height=75>
                <td width="150" ><s:property value="chMsgMap['fund.name']" /></td>
                <td>
                    <select onchange="selectFundName(this);" >
                        <option value=""></option>
                        <s:iterator value="fundType" id="fund">
                            <option value='<s:property value="fundName" />'><s:property value="fundName" /></option>
                        </s:iterator>
                    </select>
                    <br><br>
                    <input id="fundName" type="text" size=25 name="fundName" placeholder="<s:property value="chMsgMap['please.insert']" /><s:property value="chMsgMap['fund.name']" />" />
                    <input id="md5FundName" type="hidden" name="md5FundName" value="" />
                </td>
            </tr>
            <tr height=30>
                <td><s:property value="chMsgMap['buy.date']" /></td>
                <td><input id="fund-buyDate" type="text" name="buyDate" placeholder="<s:property value="chMsgMap['please.insert']" />" /></td>
            </tr>
            <tr height=30>
                <td><s:property value="chMsgMap['buy.value']" /></td>
                <td><input id="buy-value" type="text" name="buyValue" placeholder="<s:property value="chMsgMap['please.insert']" />" /></td>
            </tr>
            <tr height=30>
                <td><s:property value="chMsgMap['fund.price']" /></td>
                <td><input id="fund-price" type="text" name="price" placeholder="<s:property value="chMsgMap['please.insert']" />" /></td>
            </tr>
            <tr height=30>
                <td><s:property value="chMsgMap['fund.unit']" /></td>
                <td><input id="fund-unit" type="text" name="unit" placeholder="<s:property value="chMsgMap['please.insert']" />" /></td>
            </tr>
            <tr height=30>
                <td><s:property value="chMsgMap['currency']" /></td>
                <td>
                    <select id="currency-select" name="currency" onchange="changeCurrency(this);" >
                        <s:iterator value="currencyType" id="currency">
                            <option value='<s:property value="currency" />'><s:property value="currencyName" /></option>
                        </s:iterator>
                    </select>
                </td>
            </tr>
            <tr height=30>
                <td><s:property value="chMsgMap['exchange']" /></td>
                <td><input id="fund-exchange" type="text" name="exchangeRate" placeholder="<s:property value="chMsgMap['please.insert']" />" /></td>
            </tr>
            <tr height=50>
                <td colspan="2" align="center"><input type="button" value="<s:property value="chMsgMap['submit']" />" onclick="buyFund();" /></td>
            </tr>
        </table>
        </s:form>
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
</body>
</html>