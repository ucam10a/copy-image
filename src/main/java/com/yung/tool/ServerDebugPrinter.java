package com.yung.tool;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class ServerDebugPrinter extends AbstractDebugPrinter {

    private Object serverLogger;
    
    @Override
    public void printMessage(String message) {
        try {
            for (Method method : serverLogger.getClass().getMethods()) {
                if ((method.getName().equals("info"))) {
                    Type[] types = method.getGenericParameterTypes();
                    if (types.length == 1) {
                        method.invoke(serverLogger, message);
                        return;
                    }
                }
            }
            throw new RuntimeException(serverLogger.getClass().getName() + " doesn't have info method");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void setServerLogger(Object serverLogger) {
        this.serverLogger = serverLogger;
    }

}
