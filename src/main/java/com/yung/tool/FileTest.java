package com.yung.tool;

import java.io.File;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FileTest {
    
    public static void main(String[] args) throws Exception {
        
        ConsoleDebugPrinter printer = new ConsoleDebugPrinter();
        String srcDir = "D:/frank/test/WEB-INF";
        String destDir = "D:/frank/test/WEB-INF-1";
        
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date dt = cal.getTime();
        
        List<String> list = new ArrayList<String>();
        FileUtil.generateFileList(FileUtil.getFile(srcDir), list);
        
        List<String> changeList = new ArrayList<String>();
        for (String f : list) {
            File srcFile = FileUtil.getFile(srcDir + "/" + f);
            BasicFileAttributes attrs = FileUtil.getAttributes(srcFile);
            long createTime = attrs.creationTime().toMillis();
            long modifyTime = attrs.lastModifiedTime().toMillis();
            if (createTime >= dt.getTime() || modifyTime >= dt.getTime()) {
                changeList.add(f);
            }
        }
        printer.printObjectParam("changeList", changeList);
        
        // delete first
        for (String f : changeList) {
            File targetFile = FileUtil.getFile(destDir + "/" + f);
            if (targetFile.exists()) {
                FileUtil.delete(targetFile.getAbsolutePath());
            }
        }
        
        // copy
        for (String f : changeList) {
            File srcFile = FileUtil.getFile(srcDir + "/" + f);
            FileUtil.fileCopy(srcFile, destDir + "/" + f);
        }
        
        System.out.println("Done!");
        
    }
}