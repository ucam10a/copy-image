package com.yung.tool;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class DebugPrinterTool {

    private Object printer;
    
    public DebugPrinterTool(Object serverLogger) {
        try {
            this.printer = Class.forName("tkss.util.debug.ServerDebugPrinter").newInstance();
            for (Method method : printer.getClass().getMethods()) {
                if ((method.getName().equals("setServerLogger"))) {
                    Type[] types = method.getGenericParameterTypes();
                    if (types.length == 1) {
                        method.invoke(printer, serverLogger);
                        return;    
                    }
                }
            }
            throw new RuntimeException("setServerLogger not found");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void printObjectParam(String objName, Object obj) {
        try {
            for (Method method : printer.getClass().getMethods()) {
                if ((method.getName().equals("printObjectParam"))) {
                    Type[] types = method.getGenericParameterTypes();
                    if (types.length == 2) {
                        method.invoke(printer, objName, obj);
                        return;    
                    }
                }
            }
            throw new RuntimeException("printObjectParam not found");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void printParam(String paramName, String value) {
        try {
            for (Method method : printer.getClass().getMethods()) {
                if ((method.getName().endsWith("printParam"))) {
                    Type[] types = method.getGenericParameterTypes();
                    if (types.length == 1) {
                        method.invoke(printer, paramName, value);
                        return;
                    }
                }
            }
            throw new RuntimeException("printParam not found");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void printMarkMessage(String message) {
        try {
            for (Method method : printer.getClass().getMethods()) {
                if ((method.getName().endsWith("printObjectParam"))) {
                    Type[] types = method.getGenericParameterTypes();
                    if (types.length == 1) {
                        method.invoke(printer, message);
                        return;
                    }
                }
            }
            throw new RuntimeException("printMarkMessage not found");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void printObjectParam(Object obj) {
        try {
            for (Method method : printer.getClass().getMethods()) {
                if ((method.getName().endsWith("printObjectParam"))) {
                    Type[] types = method.getGenericParameterTypes();
                    if (types.length == 1) {
                        method.invoke(printer, obj);
                        return;
                    }
                }
            }
            throw new RuntimeException("printObjectParam not found");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
}
