package com.yung.tool;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {

    private static final Logger logger = LoggerFactory.getLogger(Config.class);
    
    private static final Map<String, String> map = new ConcurrentHashMap<String, String>();
    
    static {
        try {
            ClassLoader loader = Config.class.getClassLoader();
            Properties prop = new Properties();
            prop.load(loader.getResourceAsStream("config.properties"));
            map.put("srcDir", prop.getProperty("srcDir"));
            map.put("destDir", prop.getProperty("destDir"));
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
        
    }
    
    public static String get(String key) {
        return map.get(key);
    }

}
