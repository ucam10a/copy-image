package com.yung.copy.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yung.copy.util.CopyFileUtil;
import com.yung.tool.Config;

public class CopyJob implements Job {

	private static final Logger logger = LoggerFactory.getLogger(CopyJob.class);
	
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        
        try {
            String srcDir = Config.get("srcDir");
            String destDir = Config.get("destDir");
            
            CopyFileUtil.copyFile(srcDir, destDir, 30);
                
        } catch (Exception e) {
        	logger.error(e.toString(), e);
            throw new JobExecutionException(e);
        }
        
    }
    
}
