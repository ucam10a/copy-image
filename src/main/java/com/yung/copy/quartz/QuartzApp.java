package com.yung.copy.quartz;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.AbstractTrigger;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Quartz app to run job
 * 
 * @author Yung Long Li
 *
 */
public class QuartzApp {

    private static final Logger logger = LoggerFactory.getLogger(QuartzApp.class);
    
    /**
     * run quartz job
     */
    public void runQuartz() {

        Scheduler scheduler = null;
        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();

            // define the job and tie it to our HelloJob class
            JobDetail copyJob = new JobDetailImpl();
            ((JobDetailImpl) copyJob).setName("copyJob");
            ((JobDetailImpl) copyJob).setGroup("group1");
            ((JobDetailImpl) copyJob).setJobClass(CopyJob.class);
            
            AbstractTrigger<?> trigger3 = new CronTriggerImpl();
            trigger3.setName("trigger3");
            trigger3.setGroup("group1");
            trigger3.setJobKey(copyJob.getKey());
            ((CronTriggerImpl) trigger3).setCronExpression("0 0 1 ? * Mon");
            
            // Tell quartz to schedule the job using our trigger
            scheduler.scheduleJob(copyJob, trigger3);
            
            logger.info("quartz start! ");

        } catch (Exception e) {
            logger.error(e.toString(), e);
        } finally {
            if (scheduler != null) {
                // try {
                // scheduler.shutdown();
                // logger.info("quartz shutdown! ");
                // } catch (SchedulerException e) {
                // e.printStackTrace();
                // }
            }
        }

    }

    /**
     * stop quartz job
     */
    public void stopQuartz() {

        Scheduler scheduler = null;

        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            if (scheduler != null) scheduler.shutdown(false);

        } catch (SchedulerException e) {
            logger.error("quartz fail!", e);
        }

    }

}