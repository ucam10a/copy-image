package com.yung.copy.util;

import java.io.File;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.yung.tool.ConsoleDebugPrinter;
import com.yung.tool.FileUtil;

public class CopyFileUtil {

    public static void copyFile(String srcDir, String destDir, int days) throws Exception {
        
        ConsoleDebugPrinter printer = new ConsoleDebugPrinter();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1 * days);
        Date dt = cal.getTime();
        
        List<String> list = new ArrayList<String>();
        List<String> targetList = new ArrayList<String>();
        FileUtil.generateFileList(FileUtil.getFile(srcDir), list);
        FileUtil.generateFileList(FileUtil.getFile(destDir), targetList);
        
        List<String> changeList = new ArrayList<String>();
        List<String> addList = new ArrayList<String>();
        for (String f : list) {
            File srcFile = FileUtil.getFile(srcDir + "/" + f);
            BasicFileAttributes attrs = FileUtil.getAttributes(srcFile);
            long createTime = attrs.creationTime().toMillis();
            //printer.printObjectParam(srcFile.getName() + " create time: ", new Date(createTime));
            long modifyTime = attrs.lastModifiedTime().toMillis();
            //printer.printObjectParam(srcFile.getName() + " modify time: ", new Date(createTime));
            if (createTime >= dt.getTime() || modifyTime >= dt.getTime()) {
                changeList.add(f);
            } else {
                if (!targetList.contains(f)) {
                    addList.add(f);
                }    
            }
        }
        
        //printer.printObjectParam("changeList", changeList);
        //printer.printObjectParam("addList", addList);
        
        // delete first
        for (String f : changeList) {
            File targetFile = FileUtil.getFile(destDir + "/" + f);
            if (targetFile.exists()) {
                FileUtil.delete(targetFile.getAbsolutePath());
            }
        }
        
        // copy
        for (String f : changeList) {
            File srcFile = FileUtil.getFile(srcDir + "/" + f);
            FileUtil.fileCopy(srcFile, destDir + "/" + f);
            printer.printMessage(destDir + "/" + f);
        }
        for (String f : addList) {
            File srcFile = FileUtil.getFile(srcDir + "/" + f);
            FileUtil.fileCopy(srcFile, destDir + "/" + f);
            printer.printMessage(destDir + "/" + f);
        }
        
        printer.printMarkMessage("done");
        
    }
    
    public static void main(String[] args) throws Exception {
        
        String srcDir = "Z:/zenfoneLive";
        String destDir = "Y:/zenfoneLive";
        int days = 30;
        
        CopyFileUtil.copyFile(srcDir, destDir, days);
        
        
    }
    
}
