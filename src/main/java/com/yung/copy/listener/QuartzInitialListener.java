package com.yung.copy.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.yung.copy.quartz.QuartzApp;

/**
 * Listener for quartz to start and stop
 * 
 * @author Yung Long Li
 *
 */
public class QuartzInitialListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        QuartzApp app = new QuartzApp();
        app.stopQuartz();
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        QuartzApp app = new QuartzApp();
        app.runQuartz();
    }

}